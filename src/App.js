import { library } from '@fortawesome/fontawesome-svg-core';
import {
  fab,
  faCcVisa,
} from '@fortawesome/free-brands-svg-icons';

import {
  faAngleRight,
  faBars,
  faCaretDown,
  faHeart,
  faPhoneSquareAlt,
  faPlaneDeparture,
  faSearch,
  faShoppingCart,
  faPlus,
  faMinus,
  faSignOutAlt,
  faAddressCard,
} from '@fortawesome/free-solid-svg-icons';

import 'bootstrap/dist/css/bootstrap.min.css';

import React, { useEffect, useState } from 'react';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { Route, Router, Switch, Redirect } from "react-router-dom";
import { createStore } from 'redux';

// Styling
import './App.css';

// Components 
import CustomNavbar from './components/CustomNavbar/';
import Footer from './components/Footer/';
import Home from './components/Home/';
import Shop from './components/Shop';
import Login from './components/Admin/Login';
import Signup from './components/Admin/Signup';
import ProductDetail from './components/ProductDetail';
import Account from './components/Account';
import ErrorMsg from './components/Common/ErrorMsg';
import PersonalCart from './components/PersonalCart';
import Wishlist from './components/Wishlist';
import CustomSnackbar from './components/Notifications/CustomSnackbar';
import PaymentSuccess from './components/Payment/PaymentSuccess';

// Mixin 
import history from './history';
import rootReducer from './reducers';
import { withAuthentication } from './components/Session';
import * as ROUTES from './constants/routes';
import {
  cartSizeReq,
  productDetailsInCartReq,
  isAuthReq,
  shippingAddressesReq,
  phoneNumberByIdReq,
} from './mixin/ApiCaller';

// Action 
import { setCartSize, renderCartItems } from './actions/Cart';
import FarmerBio from './components/FarmerBio/';
import { reset } from './actions/State';
import { setShippingAddress, updateUserInfo } from './actions/User';

library.add(
  fab,
  faCaretDown,
  faSearch,
  faHeart,
  faShoppingCart,
  faPhoneSquareAlt,
  faPlaneDeparture,
  faSearch,
  faAngleRight,
  faBars,
  faPlus,
  faMinus,
  faCcVisa,
  faSignOutAlt,
  faAddressCard
);

const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const SnackbarMsg = (props) => {
  const snackbar = useSelector(state => state.initState["snackbar"]);

  return (
    snackbar &&
    <CustomSnackbar
      duration={2000}
      severity={snackbar.severity}
      msg={snackbar.msg} />
  )
}

const LoadStates = props => {
  const dispatch = useDispatch()
  const { loggedIn } = useSelector(state => state.user)

  // Initialise states
  useEffect(() => {
    isAuthReq().then(isAuthed => {
      // Cannot just use props.authUser to check if a user  
      // is authenticated or not. authUser may already be logged in
      // on firebase, but it may take a bit more time for my backend to know
      // that a user is logged in 
      if (loggedIn || isAuthed) {
        cartSizeReq().then(size => dispatch(setCartSize(size)))
        productDetailsInCartReq().then(cartItems => dispatch(renderCartItems(cartItems)))
        shippingAddressesReq().then(addresses => dispatch(setShippingAddress(addresses)))
        phoneNumberByIdReq().then(res => {
          dispatch(updateUserInfo({
            "phoneNumber": res.phone
          }))
        })
      } else {
        dispatch(reset())
      }
    })
  }, [loggedIn, dispatch, props.authUser])

  return null
}

const ProtectedRoutes = (props) => {
  return (
    <Switch>
      <Route path={ROUTES.SHOP} component={Shop} />
      <Route path={ROUTES.PRODUCT_DETAIL} component={ProductDetail} />
      <Route path={ROUTES.FARMER_BIO} component={FarmerBio} />
      <Route path={ROUTES.ACCOUNT} component={Account} />
      <Route path={ROUTES.SHOPPING_CART} component={PersonalCart} />
      <Route path={ROUTES.WISHLIST} component={Wishlist} />
      <Route path={ROUTES.PAYMENT_SUCCESS} component={PaymentSuccess} />
      <Route render={() => <ErrorMsg msg="404 Page does not exist" />} />
    </Switch>
  )
}

/**
 * We need to make sure to wait for the authorization 
 * status to come back from the server in order 
 * to make any judgements about whether the user is authenticated or not 
 * Otherwise it will redirect to login at all times because it hasn't 
 * gotten back auth result from the backend yet.
 * @param {*} props 
 */
const RestrictAccess = (props) => {
  const [isAuthed, setIsAuthed] = useState(null)

  useEffect(() => {
    isAuthReq().then(isAuthed => {
      setIsAuthed(isAuthed)
    })
  }, [isAuthed])

  return (
    <div>
      {isAuthed === true && <ProtectedRoutes />}
      {isAuthed === false && <Redirect to={ROUTES.LOGIN} />}
    </div>
  )
}


const App = (props) => {
  return (
    <Provider store={store}>
      <div style={{ height: '100%' }}>
        <CustomNavbar />
        <LoadStates />
        <main>
          <SnackbarMsg />
          <Router history={history}>
            <Switch>
              <Route exact path={ROUTES.HOME} component={Home} />
              <Route path={ROUTES.LOGIN} component={Login} />
              <Route path={ROUTES.SIGN_UP} component={Signup} />
              <RestrictAccess />
            </Switch>
          </Router>
        </main>
        <Footer />
      </div >
    </Provider>
  )
}

export default withAuthentication(App);