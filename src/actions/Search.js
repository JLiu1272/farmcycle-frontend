import * as ACTIONS from '../constants/action_types';

export const updateSearchword = (searchword) => {
    return {
        type: ACTIONS.UPDATE_SEARCHWORD,
        searchword,
    }
}