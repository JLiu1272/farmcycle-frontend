/**
 * This action file is for the initial states. 
 * 
 * Current States include 
 * --------------------------
 * openDrawer - Keeps track of whether the mobile slide bar is open or not  
 * snackbarState - The notification message on the top center  
 */
import * as ACTIONS from '../constants/action_types';

export const setDrawerState = (drawerState) => {
    return {
        type: ACTIONS.SET_DRAWER_STATE,
        drawerState,
    }
}

export const setSnackbarState = (snackbar) => {
    return {
        type: ACTIONS.SET_SNACKBAR_STATE,
        snackbar,
    }
}

export const reset = () => {
    return {
        type: ACTIONS.RESET
    }
}