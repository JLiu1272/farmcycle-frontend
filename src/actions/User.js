import * as ACTIONS from '../constants/action_types';

export const updateUserInfo = (newInfo) => {
    return {
        type: ACTIONS.UPDATE_USERINFO,
        newInfo
    }
}

export const setIsAuthedStatus = (isAuthed) => {
    return {
        type: ACTIONS.SET_AUTH_STATUS,
        isAuthed
    }
}

export const addShippingAddress = (newAddress) => {
    return {
        type: ACTIONS.ADD_SHIPPING_ADDRESS,
        newAddress
    }
}

export const removeShippingAddress = (addressLine1) => {
    return {
        type: ACTIONS.REMOVE_SHIPPING_ADDRESS,
        addressLine1
    }
}

export const setShippingAddress = (addresses) => {
    return {
        type: ACTIONS.SET_SHIPPING_ADDRESS,
        addresses
    }
}

export const setAccountActiveTab = (activeTab) => {
    return {
        type: ACTIONS.SET_ACCOUNT_ACTIVE_TAB,
        activeTab
    }
}

export const setDeliveryOption = (deliveryOption) => {
    return {
        type: ACTIONS.SET_DELIVERY_OPTION,
        deliveryOption
    }
}

export const setDeliveryAddr = (deliveryAddr) => {
    return {
        type: ACTIONS.SET_DELIVERY_ADDR,
        deliveryAddr
    }
}

export const setDeliveryTime = (deliveryTime) => {
    return {
        type: ACTIONS.SET_DELIVERY_TIME,
        deliveryTime
    }
}

export const setSellerNote = (sellerNote) => {
    return {
        type: ACTIONS.SET_SELLER_NOTE,
        sellerNote
    }
}