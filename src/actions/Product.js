
import * as ACTIONS from '../constants/action_types';

export const setProducts = (products) => {
    return {
        type: ACTIONS.SET_PRODUCTS,
        products
    }
}