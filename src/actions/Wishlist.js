import * as ACTIONS from '../constants/action_types';

export const addToWishlist = (product_id) => {
    return {
        type: ACTIONS.ADD_TO_WISHLIST,
        product_id,
    }
}

export const removeFromWishlist = (product_id) => {
    return {
        type: ACTIONS.REMOVE_FROM_WISHLIST,
        product_id,
    }
}