import * as ACTIONS from '../constants/action_types';

export const addToCart = (product_id, quantity) => {
    return {
        type: ACTIONS.ADD_TO_CART,
        product_id,
        quantity
    }
}

export const removeFromCart = (product_id, quantity) => {
    return {
        type: ACTIONS.REMOVE_FROM_CART,
        product_id,
        quantity
    }
}

export const renderCartItems = (products) => {
    return {
        type: ACTIONS.RENDER_CART_ITEMS,
        products
    }
}

export const resetTmpQuantity = () => {
    return {
        type: ACTIONS.RESET_TMP_QUANTITY
    }
}

export const updateQuantity = (new_quantity) => {
    return {
        type: ACTIONS.UPDATE_QUANTITY,
        new_quantity,
    }
}

export const setCartSize = (size) => {
    return {
        type: ACTIONS.SET_CART_SIZE,
        size,
    }
}