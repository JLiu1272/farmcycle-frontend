const baseAPI_URL = process.env.REACT_APP_API_DOMAIN + "/api/"

// Authentications
export const auth = baseAPI_URL + "auth";
export const register = baseAPI_URL + "register";
export const login = baseAPI_URL + "login";
export const logout = baseAPI_URL + "logout";
export const isAuth = baseAPI_URL + "isAuth";

// Carts 
export const addToCart = baseAPI_URL + "addToCart";
export const removeFromCart = baseAPI_URL + "removeFromCart";
export const cartByCustomerId = baseAPI_URL + "cartByCustomerId";
export const productDetailsInCart = baseAPI_URL + "productDetailsInCart";
export const updateCartQuantity = baseAPI_URL + "updateCartQuantity";
export const cartSize = baseAPI_URL + "cartSize";

// Wishlist  
export const removeFromWishlist = baseAPI_URL + "removeFromWishlist";
export const addToWishlist = baseAPI_URL + "addToWishlist";
export const isFavorited = baseAPI_URL + "isFavorited";
export const wishlistByCustomerId = baseAPI_URL + "wishlistByCustomerId";

// Load Products 
export const allProductsByTag = baseAPI_URL + "allProductsByTag";
export const productsByIds = baseAPI_URL + "productsByIds";
export const productsByKeyword = baseAPI_URL + "productsByKeyword";

// Farmers 
export const farmerInfoById = baseAPI_URL + "farmInfoById";

// Customer Profile 
export const addShippingAddress = baseAPI_URL + "addShippingAddress"
export const shippingAddresses = baseAPI_URL + "shippingAddresses"
export const removeShippingAddress = baseAPI_URL + "removeShippingAddress"
export const updatePhoneNumber = baseAPI_URL + "updatePhoneNumber"
export const phoneNumberById = baseAPI_URL + "phoneNumberById"
export const confirmCheckout = baseAPI_URL + "confirmCheckout"

// Payment 
export const makeCheckoutSession = baseAPI_URL + "makeCheckoutSession"
