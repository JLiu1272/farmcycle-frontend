// Global State Actions
export const SET_DRAWER_STATE = "SET_DRAWER_STATE";
export const SET_SNACKBAR_STATE = "SET_SNACKBAR_STATE";
export const RESET = "RESET"

// Cart Actions 
export const ADD_TO_CART = "ADD_TO_CART";
export const REMOVE_FROM_CART = "REMOVE_FROM_CART";
export const UPDATE_QUANTITY = "UPDATED_QUANTITY";
export const RENDER_CART_ITEMS = "RENDER_CART_ITEMS";
export const SET_CART_SIZE = "SET_CART_SIZE";
export const RESET_TMP_QUANTITY = "RESET_TMP_QUANTITY";

// Product 
export const ADD_TO_WISHLIST = "ADD_TO_WISHLIST";
export const REMOVE_FROM_WISHLIST = "REMOVE_FROM_WISHLIST";
export const SET_PRODUCTS = "SET_PRODUCTS";

// Search Actions 
export const UPDATE_SEARCHWORD = "UPDATE_SEARCHWORD";

// User Profile 
export const UPDATE_USERINFO = "UPDATE_USERINFO";
export const SET_AUTH_STATUS = "SET_AUTH_STATUS"
export const ADD_SHIPPING_ADDRESS = "ADD_SHIPPING_ADDRESS"
export const REMOVE_SHIPPING_ADDRESS = "REMOVE_SHIPPING_ADDRESS"
export const SET_SHIPPING_ADDRESS = "SET_SHIPPING_ADDRESS"
export const SET_ACCOUNT_ACTIVE_TAB = "SET_ACCOUNT_ACTIVE_TAB"
export const SET_DELIVERY_OPTION = "SET_DELIVERY_OPTION"
export const SET_DELIVERY_ADDR = "SET_DELIVERY_ADDR"
export const SET_DELIVERY_TIME = "SET_DELIVERY_TIME"
export const SET_SELLER_NOTE = "SET_SELLER_NOTE"
