import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#28A745',
        }
    }
})

const colorPalette = {
    primary: '#158533'
}

export default theme;

export { colorPalette };