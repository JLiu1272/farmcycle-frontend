export const HOME = "/";
export const SIGN_UP = "/signup";
export const LOGIN = "/login";
export const SHOP = "/shop";
export const ABOUT_US = "/about_us";
export const SPECIALS = "/specials";
export const BLOGS = "/blogs";
export const SERVICE = "/service";
export const ACCOUNT = "/account";
export const SHOPPING_CART = "/cart";
export const WISHLIST = "/wishlist";
export const PAYMENT_SUCCESS = "/payment_success";

// Dynamic Renderings. Routes that change 
// based on the parameter that is passed in 
export const PRODUCT_DETAIL = "/product/:productId";

// Farmer Bio 
export const FARMER_BIO = "/farmer/:farmerId";

export const domain = "http://localhost:3000"