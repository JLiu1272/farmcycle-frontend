import * as ACTIONS from '../constants/action_types';

export default function (state = {
    "openDrawer": false,
    "snackbar": null,
}, action) {
    switch (action.type) {
        case ACTIONS.SET_DRAWER_STATE:
            return { ...state, "openDrawer": action.drawerState };
        case ACTIONS.SET_SNACKBAR_STATE:
            return { ...state, "snackbar": action.snackbar }
        default:
            return state;
    }
}