import * as ACTIONS from '../constants/action_types';

export default function (state = "", action) {
    switch (action.type) {
        case ACTIONS.UPDATE_SEARCHWORD:
            return action.searchword;
        default:
            return state;
    }
}