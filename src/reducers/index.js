import { combineReducers } from 'redux';
import ProductReducer from './ProductReducer';
import CartReducer from './CartReducer';
import StaticImgReducer from './StaticImgReducer';
import FilterCategoryReducer from './FilterCategoryReducer';
import SearchReducer from './SearchReducer';
import UserReducer from './UserReducer';
import initState from './initState';
import DeliveryOptionsReducer from './DeliveryOptionsReducer';

import * as ACTIONS from '../constants/action_types'

const allReducers = combineReducers({
    inventory: ProductReducer,
    cart: CartReducer,
    staticImgs: StaticImgReducer,
    filterCategory: FilterCategoryReducer,
    searchword: SearchReducer,
    user: UserReducer,
    initState,
    deliveryOptions: DeliveryOptionsReducer,
});

const rootReducer = (state, action) => {
    if (action.type === ACTIONS.RESET) { state = undefined }
    return allReducers(state, action)
}

export default rootReducer;