import * as ACTIONS from '../constants/action_types';

export default function (state = {
    displayName: "",
    email: "",
    phoneNumber: "",
    addresses: null,
    loggedIn: false,
    accountActiveTab: 0,
    deliveryOption: "pickup",
    deliveryAddr: "Public Farmers Market",
    deliveryTime: "",
    sellerNote: "",
}, action) {
    switch (action.type) {
        case ACTIONS.UPDATE_USERINFO:
            return {
                ...state,
                ...action.newInfo
            }
        case ACTIONS.SET_AUTH_STATUS:
            return {
                ...state,
                loggedIn: action.isAuthed
            }
        case ACTIONS.ADD_SHIPPING_ADDRESS:
            return {
                ...state,
                addresses: {
                    ...state.addresses,
                    [action.newAddress.addressLine1]: action.newAddress
                }
            }
        case ACTIONS.REMOVE_SHIPPING_ADDRESS:
            if (state.addresses.hasOwnProperty(action.addressLine1)) {
                delete state.addresses[action.addressLine1]
            }
            return {
                ...state,
                addresses: state.addresses
            }
        case ACTIONS.SET_SHIPPING_ADDRESS:
            return {
                ...state,
                addresses: action.addresses
            }
        case ACTIONS.SET_ACCOUNT_ACTIVE_TAB:
            // This is used so that I can use names to set 
            // the active tab, but it will automatically convert it 
            // to index when stored in the redux store
            const activeTabLookupTable = {
                "profile": 0,
                "address": 1,
            }
            let newActiveTab = action.activeTab in activeTabLookupTable
                ? activeTabLookupTable[action.activeTab]
                : action.activeTab

            return {
                ...state,
                accountActiveTab: newActiveTab
            }
        case ACTIONS.SET_DELIVERY_OPTION:
            return {
                ...state,
                deliveryOption: action.deliveryOption
            }
        case ACTIONS.SET_DELIVERY_ADDR:
            return {
                ...state,
                deliveryAddr: action.deliveryAddr
            }
        case ACTIONS.SET_DELIVERY_TIME:
            return {
                ...state,
                deliveryTime: action.deliveryTime
            }
        case ACTIONS.SET_SELLER_NOTE:
            return {
                ...state,
                sellerNote: action.sellerNote
            }
        default:
            return state
    }
}