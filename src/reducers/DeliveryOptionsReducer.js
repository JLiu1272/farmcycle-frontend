export default function (state = {
    farmersMarket: ["Public Farmers Market", "Brighton Farmers Market"],
    deliveryTimes: [
        "Within 4 hours",
        "Within 6 hours",
        "Within 7 hours"
    ]
}, action) {
    switch (action.type) {
        default:
            return state
    }
}