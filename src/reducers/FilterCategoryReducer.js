const FilterCategories = () => {
    return [
        {
            id: "root1",
            name: "Bakery",
            children: [
                {
                    id: "root1_1",
                    name: "Multi-Grain"
                },
                {
                    id: "root1_2",
                    name: "Pita Bread",
                    children: [
                        {
                            id: "root1_3",
                            name: "Soft Pita Bread"
                        }
                    ]
                }
            ]
        },
        {
            id: "root2",
            name: "Fish",
            children: [
                {
                    id: "root2_1",
                    name: "Salmon"
                },
                {
                    id: "root2_2",
                    name: "Scallop",
                    children: [
                        {
                            id: "root2_3",
                            name: "Coconut"
                        }
                    ]
                }
            ]
        }
    ]
}

export default FilterCategories; 