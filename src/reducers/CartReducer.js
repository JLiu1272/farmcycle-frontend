import * as ACTIONS from '../constants/action_types';

export default function (state = {
    size: 0,
    cartItems: {},
    tmp_quantity: 1,
}, action) {
    switch (action.type) {
        case ACTIONS.SET_CART_SIZE:
            return {
                ...state,
                size: action.size
            }
        case ACTIONS.RESET_TMP_QUANTITY:
            return {
                ...state,
                tmp_quantity: 1
            }
        case ACTIONS.RENDER_CART_ITEMS:
            return {
                ...state,
                cartItems: action.products,
            }
        case ACTIONS.ADD_TO_CART:
            const { quantity } = action;
            const { cartItems } = state;
            return {
                ...state,
                cartItems: {
                    ...state.cartItems,
                    [action.product_id]: action.product_id in cartItems
                        ? cartItems[action.product_id] + quantity
                        : quantity
                },
                size: state["size"] + quantity
            }
        case ACTIONS.REMOVE_FROM_CART:
            let cartItemsClone = Object.assign({}, state.cartItems)
            delete cartItemsClone[action.product_id];
            return {
                ...state,
                cartItems: cartItemsClone,
                size: state["size"] - action.quantity
            };
        case ACTIONS.UPDATE_QUANTITY:
            const { new_quantity } = action;
            return {
                ...state,
                tmp_quantity: new_quantity,
            }
        default:
            return state;
    }
}