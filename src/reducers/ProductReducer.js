import * as ACTIONS from '../constants/action_types';
import _ from 'lodash';

export default function (state = {
    products: {},
    wishlist: [],
}, action) {
    switch (action.type) {
        case ACTIONS.SET_PRODUCTS:
            return {
                ...state,
                products: action.products
            }
        case ACTIONS.ADD_TO_WISHLIST:
            const { product_id } = action;
            return {
                ...state,
                wishlist: [...new Set([...state.wishlist, product_id])],
                products: {
                    ...state.products,
                    [product_id]: {
                        ...state.products[product_id],
                        wishlist: !state.products[product_id].wishlist
                    }
                }
            }
        case ACTIONS.REMOVE_FROM_WISHLIST:
            return {
                ...state,
                wishlist: _.filter(state.wishlist, id => id !== action.product_id),
                products: {
                    ...state.products,
                    [action.product_id]: {
                        ...state.products[action.product_id],
                        wishlist: !state.products[action.product_id].wishlist
                    }
                }
            }
        default:
            return state
    }
}