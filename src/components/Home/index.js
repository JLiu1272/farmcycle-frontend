import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Badge from 'react-bootstrap/Badge';
import Box from '@material-ui/core/Box'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import ItemsCarousel from 'react-items-carousel';
import _ from 'lodash';

// Components
import Delivery from '../Delivery/'
import Products from '../Product/Products';
import ImageSlider from '../ImageSlider';
import Advertisement from '../Advertisement';
import FullWidthAd from '../Advertisement/FullWidthAd';

// CSS
import './Recipes.css';
import '../../assets/common.css';

const recipes = [
    {
        "id": 2891,
        "date": "06/04/2020",
        "author": "Jennifer",
        "title": "Oatmeal Recipe",
        "ingredients": ["Oatmeal", "Raspberry", "Milk", "Berries", "Brown Sugar"],
        "img": require('../../assets/img/recipes/oatmeal.jpg'),
    },
    {
        "id": 2892,
        "date": "06/04/2020",
        "author": "Jennifer",
        "title": "Oatmeal Recipe",
        "ingredients": ["Oatmeal", "Raspberry", "Milk", "Berries", "Brown Sugar"],
        "img": require('../../assets/img/recipes/oatmeal.jpg'),
    },
    {
        "id": 2893,
        "date": "06/04/2020",
        "author": "Jennifer",
        "title": "Oatmeal Recipe",
        "ingredients": ["Oatmeal", "Raspberry", "Milk", "Berries", "Brown Sugar", "Milk", "Milk", "Milk"],
        "img": require('../../assets/img/recipes/oatmeal.jpg'),
    }
];

const Recipe = props => {
    let styles = {
        recipe_card_container: {
            width: '20rem',
            margin: '0 auto',
        },
        show_more_icon: {
            margin: 5,
        },
        ingredient_container: {
            display: "flex",
            flexWrap: "wrap",
            paddingBottom: 10,
        }
    }

    let recipe = props.recipe;
    return (
        <Card key={recipe.id} style={styles.recipe_card_container}>
            <Card.Img variant="top" src={recipe.img} />
            <Card.Body>
                <Card.Text>{recipe.date} | Author: {recipe.author}</Card.Text>
                <Card.Title>{recipe.title}</Card.Title>
                <div>Uses up: </div>
                <div style={styles.ingredient_container}>
                    {recipe.ingredients.map((ingredient, index) => {
                        return <Badge key={`${ingredient}-${recipe.id}-${index}`} className="ingredients" variant="light">{ingredient}</Badge>
                    })}
                </div>
                <a href="/" className="show_more_btn">
                    Show more
                    <FontAwesomeIcon style={styles.show_more_icon} color="green" icon="angle-right" />
                </a>
            </Card.Body>
        </Card>
    );

}
const RecipeCarousel = (props) => {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const [numCards, setNumCards] = useState(3);
    const chevronWidth = 40;

    let styles = {
        recipe_cards_container: {
            padding: `0 ${chevronWidth}px`,
        }
    }

    // Call this the first time the page was loaded
    const useMountEffect = (adjustNumCardsFunc) => useEffect(adjustNumCardsFunc, []);

    const debouncedHandleResize = _.debounce(
        function handleResize() {
            let width = window.innerWidth;
            if (width <= 800) {
                setNumCards(1);
            }
            else if (width <= 1200) {
                setNumCards(2);
            }
            else {
                setNumCards(3);
            }
        }, 300);

    // The first time the webpage is loaded
    useMountEffect(debouncedHandleResize);

    useEffect(() => {
        window.addEventListener('resize', debouncedHandleResize)

        // Cleanup event listener. Prevent memory leak in program 
        return _ => {
            window.removeEventListener('resize', debouncedHandleResize)
        }
    })

    return (
        <div style={styles.recipe_cards_container}>
            <ItemsCarousel
                requestToChangeActive={setActiveItemIndex}
                activeItemIndex={activeItemIndex}
                numberOfCards={numCards}
                gutter={25}
                infiniteLoop={true}
                leftChevron='<'
                rightChevron='>'
                outsideChevron
                chevronWidth={chevronWidth}
            >
                {recipes.map(recipe => {
                    return (
                        <Recipe key={recipe.id} recipe={recipe} />
                    )
                })}
            </ItemsCarousel>
        </div>
    );
}

const Recipes = props => {
    return (
        <div className="recipes_container">
            <div className="product_title">
                <p className="subtitle">Use up leftovers</p>
                <p className="title">Recommended Recipes</p>
            </div>
            <RecipeCarousel />
        </div>
    );
};

const Home = (props) => {
    return (
        <div>
            <ImageSlider />
            <Delivery />
            <Box mt={6}>
                <Products
                    title="Deals Of The Week"
                    subtitle="Recently added to our store"
                    showTab={false}
                    rowNum={1} />
                <Advertisement numAds={2} />
                <Products
                    title="Trending Products"
                    subtitle="Recently added to our store"
                    showTab={true}
                    rowNum={2} />
                <FullWidthAd />
                <Recipes />
            </Box>
        </div>
    );
};

export default Home;