import React from 'react';
import Box from '@material-ui/core/Box';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { useMediaQuery } from '@material-ui/core';

import SelectDeliveryLocation from './SelectDeliveryLocation';
import DeliveryTimes from './DeliveryTimes';
import theme from '../../constants/theme'

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: "#e8f3e7",
        minHeight: "5rem",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "center",
        top: 0,
        zIndex: 4
    }
}));

const Delivery = props => {
    const classes = useStyles()
    // // Using media query hook to determine if screen is too small, 
    // // and add padding accordingly 
    const mobileSize = useMediaQuery('(max-width:400px)')

    return (
        <ThemeProvider theme={theme}>
            <Box p={2} className={classes.root} boxShadow={2} position={mobileSize ? 'relative' : 'sticky'}>
                <SelectDeliveryLocation />
                <DeliveryTimes />
            </Box>
        </ThemeProvider>
    );
};


export default Delivery;