import React, { useState, useEffect, useCallback } from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';

import { setDeliveryTime } from '../../actions/User'

const useStyles = makeStyles(theme => {
    return {
        dialog_root: {
            alignText: "center",
            backgroundColor: theme.palette.primary.main,
            color: "white"
        }
    }
})

const DeliveryTimes = props => {
    const classes = useStyles()
    const theme = useTheme();
    const dispatch = useDispatch()
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const { deliveryTimes } = useSelector(state => state.deliveryOptions)
    const { deliveryTime } = useSelector(state => state.user)
    const [open, setOpen] = useState(false);

    const initDeliveryTime = useCallback(
        () => {
            // Only initialise if no delivery time is chosen
            if (deliveryTime === "") {
                dispatch(setDeliveryTime(deliveryTimes[0]))
            }
        },
        [dispatch, deliveryTimes, deliveryTime],
    )

    useEffect(() => {
        initDeliveryTime()
    }, [initDeliveryTime])

    const handleChange = (newTime) => {
        dispatch(setDeliveryTime(newTime))
        setOpen(false)
    }

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <Box display="flex" alignItems="center" m={2}>
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                {deliveryTime}
            </Button>
            <Dialog
                fullScreen={fullScreen}
                open={open}
                onClose={handleClose}
                aria-labelledby="delivery-time-dialog"
            >
                <DialogTitle
                    id="delivery-time-dialog"
                    classes={{
                        root: classes.dialog_root
                    }}
                >
                    Select Delivery Time
                    </DialogTitle>
                <List>
                    {deliveryTimes.map((time) => (
                        <ListItem button onClick={() => handleChange(time)} key={time}>
                            <ListItemText primary={time} />
                        </ListItem>
                    ))}
                </List>
            </Dialog>
        </Box>
    );
};


export default DeliveryTimes;