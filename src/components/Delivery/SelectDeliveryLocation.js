import React, { useState, useEffect, useCallback } from 'react';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { useMediaQuery } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux'
import _ from 'lodash'

// Reducers 
import { setDeliveryOption, setDeliveryAddr } from '../../actions/User'

const useStyles = makeStyles((theme) => ({
    formControl: {
        minWidth: 300,
        marginLeft: 20,
    },
    option_btn: {
        height: "3.1rem",
        marginTop: 8
    },
}));

const SelectDeliveryLocation = props => {
    const classes = useStyles()
    const dispatch = useDispatch()

    const { addresses } = useSelector(state => state.user)
    const { deliveryOption, deliveryAddr } = useSelector(state => state.user)
    const { farmersMarket } = useSelector(state => state.deliveryOptions)

    // Obtain the address line 1 from the full 
    // address, so we can display a short name 
    // in the address dropdown selection
    const shortenAddresses = useCallback(() => {
        return _.reduce(addresses, (result, value, key) => {
            result.push(key)
            return result
        }, [])
    }, [addresses])

    const initLocation = () => {
        // Not using pickupChosen because I will have to 
        // make pickupChosen a callback as well
        switch (deliveryOption) {
            case "pickup":
                return farmersMarket
            case "delivery":
                return shortenAddresses()
            default:
                return []
        }
    }

    // This is needed because the locations will differ depending on 
    // whether user is picking up or delivering the item
    const [locations, setLocation] = useState(initLocation())

    // Using media query hook to determine if screen is too small, 
    // and add padding accordingly 
    const mobileSize = useMediaQuery('(max-width:400px)')

    const pickupChosen = () => {
        return deliveryOption === "pickup"
    }

    useEffect(() => {
        switch (deliveryOption) {
            case "pickup":
                setLocation(farmersMarket)
                return
            case "delivery":
                setLocation(shortenAddresses())
                return
            default:
                return
        }

    }, [deliveryOption, farmersMarket, shortenAddresses])

    const handleChange = (event) => {
        dispatch(setDeliveryAddr(event.target.value))
    }

    return (
        <Box display="flex" flexWrap="wrap" justifyContent="space-around">
            <ButtonGroup size="small" color="primary" className={classes.option_btn}>
                <Button
                    aria-label="pickup"
                    variant={pickupChosen() ? "contained" : "outlined"}
                    onClick={() => dispatch(setDeliveryOption("pickup"))}
                >
                    Pick Up
                    </Button>
                <Button
                    aria-label="delivery"
                    onClick={() => dispatch(setDeliveryOption("delivery"))}
                    variant={pickupChosen() ? "outlined" : "contained"}
                >
                    Delivery
                    </Button>
            </ButtonGroup>
            <Box mt={mobileSize ? 2 : 0}>
                <FormControl variant="filled" className={classes.formControl} margin="dense" size="small">
                    <Select
                        native
                        value={deliveryAddr}
                        onChange={handleChange}
                        inputProps={{
                            name: 'address',
                            id: 'address-native-simple',
                        }}
                    >
                        {_.size(locations) === 0 &&
                            <option value="Please add delivery address">No delivery addresses available</option>
                        }
                        {locations.map((location, index) => {
                            return <option key={`${index}-location`} value={location}>{location}</option>
                        })}
                    </Select>
                </FormControl>
            </Box>
        </Box>
    );
};

export default SelectDeliveryLocation;