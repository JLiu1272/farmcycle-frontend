import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import './ImageSlider.css';

const images = [1, 2, 3];

class ImageSlider extends Component {
    render() {
        return (
            <Carousel>
                {images.map((index) =>
                    <Carousel.Item key={index}>
                        <div className="slider_content"></div>
                    </Carousel.Item>
                )}
            </Carousel>

        );
    }
}

export default ImageSlider;