import React from 'react';

const SmallLogo = props => {
    return (
        <img
            src={require('../../assets/logos/FC_Icon.svg')}
            alt="Farm Cycle Logo"
            height={props.height}
        />
    )
}

export default SmallLogo