import React from 'react';
import Box from '@material-ui/core/Box'
import { makeStyles } from '@material-ui/core';

const LargeLogo = () => {
    const useStyle = makeStyles({
        container: {
            backgroundImage: `url(${require('../../assets/logos/FC_Beige.svg')})`,
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
            backgroundColor: "#F6CFB0",
            height: 300
        }
    })
    const classes = useStyle()

    return (
        <Box component="div" className={classes.container}></Box>
    )
}

export default LargeLogo