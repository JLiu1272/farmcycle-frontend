import React from 'react';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Box from '@material-ui/core/Box'

// Components
import SmallLogo from '../Logo/SmallLogo';

// Constants 
import * as CONTACTS from '../../constants/contacts'

// CSS
import './Footer.css';
import '../../assets/common.css';

const footerContents = [
    {
        "header": "Information",
        "details": [
            "About Us",
            "Delivery Information",
            "Privacy Policy",
            "Terms & Conditions",
            "Contact Us",
            "Site Map",
        ]
    },
    {
        "header": "Extras",
        "details": [
            "Brands",
            "Gift Certificates",
            "Affiliate",
            "Specials",
            "Returns",
            "Order History",
        ]
    }
];

const Info = (props) => {
    return (
        <div className="info_container">
            <h4>{props.content.header}</h4>
            <ul>
                {
                    props.content.details.map((link, index) => {
                        return (
                            <li key={`${link}${index}`}><a href="/">{link}</a></li>
                        );
                    })

                }
            </ul>
        </div>
    );
}

const Footer = props => {
    return (
        <div>
            <div className="footer_container">
                <div>
                    <SmallLogo height="120" />
                    <Box className="contact_info" p={1} lineHeight="2em" textAlign="center">
                        Email: <a href={`mailto:${CONTACTS.helloEmail}`}>hello@farmcycle.com</a>
                        <br />
                        Call us: <a href={`tel:${CONTACTS.customerServicePhone}`}>(+1) 413 559 1042</a>
                    </Box>
                </div>
                {
                    footerContents.map((content, index) => {
                        return (
                            <Info key={`${content.header}${index}`} content={content} />
                        )
                    })
                }
                <div>
                    <h4>Signup For Newsletter</h4>
                    <p> Get updates by subscribing to our weekly newsletter</p>
                    <InputGroup className="mb-3">
                        <FormControl
                            placeholder="email@example.com"
                            aria-label="email@example.com"
                            aria-describedby="basic-addon2"
                        />
                        <InputGroup.Append>
                            <Button variant="success">Subscribe</Button>
                        </InputGroup.Append>
                    </InputGroup>
                </div>
            </div>
            <div className="copyright_container">
                <p>&copy; Farm Cycle {new Date().getFullYear()}</p>
            </div>
        </div>
    );
};

export default Footer;