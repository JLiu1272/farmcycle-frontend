import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';

// Components 
import ProductToRow from './Common/ProductToRow';
import theme from '../constants/theme';

// API 
import { wishlistByCustomerIdReq } from '../mixin/ApiCaller';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        padding: "10px"
    },
}));

const FavoriteGroceries = (props) => {
    const [wishlist, setWishlist] = useState([]);

    // Get the list of wishlist for the customer 
    useEffect(() => {
        wishlistByCustomerIdReq().then(setWishlist);
    }, [])

    const renderFavorites = () => {
        return <ProductToRow numCols={4} products={wishlist} />
    }

    return (
        <div>
            {wishlist.length === 0 &&
                <p>
                    No Favorite Items, Add now!
                </p>
            }
            {wishlist.length > 0 &&
                renderFavorites()
            }
        </div>
    )
}

const FavoriteFarmers = (props) => {
    const farmers = [];

    return (
        <div>
            {farmers.length === 0 &&
                <p>
                    No Favorite Farmers, Add now!
                </p>
            }
        </div>
    )
}

const Wishlist = props => {
    const classes = useStyles();
    const [value, setValue] = useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const handleChangeIndex = (index) => {
        setValue(index);
    };

    return (
        <ThemeProvider theme={theme}>
            <div className={classes.root}>
                <AppBar position="static" color="default">
                    <Tabs
                        value={value}
                        onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        variant="fullWidth"
                        aria-label="full width tabs example"
                    >
                        <Tab label="Favorite Groceries" {...a11yProps(0)} />
                        <Tab label="Favorite Farmers" {...a11yProps(1)} />
                    </Tabs>
                </AppBar>
                <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={value}
                    onChangeIndex={handleChangeIndex}
                >
                    <TabPanel value={value} index={0} dir={theme.direction}>
                        <FavoriteGroceries />
                    </TabPanel>
                    <TabPanel value={value} index={1} dir={theme.direction}>
                        <FavoriteFarmers />
                    </TabPanel>
                </SwipeableViews>
            </div>
        </ThemeProvider>

    );
}

Wishlist.propTypes = {

};

export default Wishlist;