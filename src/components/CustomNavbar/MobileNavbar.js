import React, { useState, useEffect } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Divider from '@material-ui/core/Divider';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';

import SmallLogo from '../Logo/SmallLogo';
import SlideBar from './Slidebar';
import history from '../../history';

import './CustomNavbar.css'
import '../../assets/common.css';

// A list of Navbar Items 
import { navbar_items } from './menu_items';

// Action 
import { setDrawerState } from '../../actions/State';

/**
 * Dropdown menu for mobile menu interface  
 * @param {} props 
 */
const MobileDropdown = (props) => {
    let styles = {
        nested: {
            paddingLeft: "11%",
        },
    };

    const dispatch = useDispatch();

    // Create a dropdown toggle state for 
    // menu items that have dropdowns. 
    const openStates = () => {
        let opens = {};
        navbar_items.forEach((item) => {
            if (item.dropdown_content) {
                opens[item.title] = false;
            }
        })
        return opens;
    }

    const [openStatus, setOpenStatus] = useState(openStates());

    const updateStates = (title) => {

        // Need to obtain the previous state first, before 
        // setting all to false. Because the dropdown list could 
        // either be at open or close state. 
        let titleNewState = !openStatus[title];

        // Set all values to false 
        let newStates = _.mapValues(openStatus, () => false);
        newStates[title] = titleNewState;

        setOpenStatus(newStates);
    }

    // When clicking on an item, 
    // close side bar and then go to page 
    const toLink = (link) => {
        dispatch(setDrawerState(false));
        history.push(link);
    }


    const ButtonItem = (props) => {
        return (
            <ListItem button onClick={() => { toLink(props.item.link) }}>
                <ListItemText primary={props.item.title} />
            </ListItem>
        )
    }

    const DropdownItems = (props) => {
        return (
            <div>
                <ListItem button onClick={() => updateStates(props.item.title)}>
                    <ListItemText primary={props.item.title} style={styles.item_text} />
                    {props.item.title in openStatus && openStatus[props.item.title] ? <ExpandLess /> : <ExpandMore />}
                </ListItem>
                <Collapse in={openStatus[props.item.title]} timeout="auto" unmountOnExit>
                    {props.item.dropdown_content.map((dropdown_title, index) => {
                        return (
                            <List component="div" disablePadding key={`${dropdown_title}-${index}`}>
                                <ListItem button style={styles.nested}>
                                    <ListItemText primary={dropdown_title} />
                                </ListItem>
                            </List>
                        )
                    })}
                </Collapse>
            </div>
        )
    }

    return props.item.dropdown_content ? <DropdownItems item={props.item} /> : <ButtonItem item={props.item} />
}

/**
 * Navigation Menu for mobile devices
 * @param {*} props 
 */
const MobileNavItems = (props) => {
    let styles = {
        root: {
            width: "100%",
        },
    };

    return (
        <List
            component="nav"
            style={styles.root}
        >
            {navbar_items.map((item, index) => {
                return (
                    <div key={`Mob-${item.title}-${index}`}>
                        <MobileDropdown item={item} />
                        <Divider variant="middle" />
                    </div>
                )
            })}
        </List>
    )
}

const MobileNavbar = (props) => {
    const { openDrawer } = useSelector(state => state.initState);
    const dispatch = useDispatch();

    const toggleSlideBar = () => {
        dispatch(setDrawerState(!openDrawer));
    }

    // When window size is greater than or equal to 920px 
    // close the drawer. 
    const debouncedHandleResize = _.debounce(
        function handleResize() {
            let width = window.innerWidth;
            if (width >= 920) {
                dispatch(setDrawerState(false));
            }
        }, 300);

    useEffect(() => {
        window.addEventListener('resize', debouncedHandleResize)

        // Cleanup event listener. Prevent memory leak in program 
        return _ => {
            window.removeEventListener('resize', debouncedHandleResize)
        }
    })

    return (
        <div>
            <SlideBar />
            <div className="mobile_container">
                <SmallLogo height="80" />
                <FontAwesomeIcon onClick={toggleSlideBar} className="mobile_nav_icon" icon="bars" />
            </div>
        </div>
    );
}

export default MobileNavbar;

export { MobileDropdown, MobileNavItems };