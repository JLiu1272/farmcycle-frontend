import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react';

// Constants 
import * as CONTACTS from '../../constants/contacts'

const CustomerSupport = (props) => {
    let stylePlugin = props.style ? props.style : {};

    return (
        <div className="customer_support_container" style={stylePlugin}>
            <FontAwesomeIcon icon="phone-square-alt" style={{ fontSize: 40 }} />
            <div className="customer_support_details">
                <a href={`tel:${CONTACTS.customerServicePhone}`}>(413)-559-1042</a>
                <br />
                <span>Customer Support</span>
            </div>
        </div>
    )
}

export default CustomerSupport; 