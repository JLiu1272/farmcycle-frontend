// Routes 
import * as ROUTES from '../../constants/routes';

export const navbar_items = [
    {
        "title": "Home",
        "link": ROUTES.HOME,
    },
    {
        "title": "Shop",
        "link": ROUTES.SHOP,
        // "dropdown_content": ["Vegetables", "Carrots"],
    },
    {
        "title": "About Us",
        "link": ROUTES.ABOUT_US,
    },
    {
        "title": "Specials",
        "link": ROUTES.SPECIALS,
    },
    {
        "title": "Blogs",
        "link": ROUTES.BLOGS,
    },
    {
        "title": "How it works",
        "link": ROUTES.SERVICE,
        "dropdown_content": ["About Us", "Service", "FAQ"]
    }
];