import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Components
import LargeLogo from '../Logo/LargeLogo';
import Searchbar from '../Searchbar';
import Admin from '../Admin/';
import CustomerSupport from './CustomerSupport';
import history from '../../history';
import MobileNavbar from './MobileNavbar';

import './CustomNavbar.css'
import '../../assets/common.css';
import { makeStyles } from '@material-ui/core';

// A list of Navbar Items 
import { navbar_items } from './menu_items';

const globalStyles = makeStyles(() => ({
    nav_links: {
        fontWeight: 500,
        cursor: "pointer",
        "&:hover": {
            color: "green"
        }
    }
}))

const DropdownMenu = (props) => {
    const globalClasses = globalStyles();

    return (
        <li>
            <div className="dropdown">
                <span
                    className={globalClasses.nav_links}
                    onClick={() => { history.push(`${props.item.link}`) }}
                >{props.item.title}</span>
                <FontAwesomeIcon className="caret_dropdown" icon="caret-down" />
                <div
                    className="dropdown-content">
                    {props.item.dropdown_content.map((content, index) => {
                        return (
                            <p key={`${content}-${index}`}>{content}</p>
                        )
                    })}
                </div>
            </div>
        </li>
    )
}

/**
 * This is nav item for desktop screens
 * @param {*} props 
 */
const NavItems = (props) => {
    const globalClasses = globalStyles();

    return (
        <div className="navbar_navigation-items navbar_toggle">
            <ul>
                {navbar_items.map((item, index) => {
                    if (item.dropdown_content) {
                        return (
                            <DropdownMenu key={`${item.title}-${index}`} item={item} />
                        )
                    } else {
                        return (
                            <li key={`${item.title}-${index}`}>
                                <span
                                    className={globalClasses.nav_links}
                                    onClick={() => { history.push(`${item.link}`) }}>
                                    {item.title}
                                </span>
                            </li>
                        )
                    }
                })}
            </ul>
        </div>
    )
}

const NavLinks = (props) => (
    <header className="navbar">
        <nav className="navbar__navigation">
            <NavItems />
            <CustomerSupport />
        </nav>
    </header>
)

const ToggleNavbar = (props) => {
    return (
        <div className="navbar_toggle">
            <LargeLogo />
            <div className="header_container">
                <Searchbar />
                <Admin />
            </div>
            <NavLinks />
        </div>
    );
}

const CustomNavbar = props => {
    return (
        <div>
            <MobileNavbar />
            <ToggleNavbar />
        </div>
    )
}

export default CustomNavbar