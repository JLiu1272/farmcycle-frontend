import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import CloseIcon from '@material-ui/icons/Close';

// Components
import Searchbar from '../Searchbar';
import Admin from '../Admin/';
import CustomerSupport from './CustomerSupport';
import { MobileNavItems } from './MobileNavbar';
import { useSelector, useDispatch } from 'react-redux';

// Actions 
import { setDrawerState } from '../../actions/State';

// CSS
import './CustomNavbar.css';
import { makeStyles } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
    close_btn: {
        marginBottom: 30,
        position: "relative",
        left: "85%",
        top: 20,
        cursor: "pointer",
        "&:hover": {
            backgroundColor: "green",
            border: "solid 1px black",
            color: "white",
            borderRadius: 20,
        }
    }
}))


/**
 * The sliding window for the navigation bar 
 * on mobile 
 * @param {} props 
 */
const SlideBar = (props) => {
    const { openDrawer } = useSelector(state => state.initState);
    const dispatch = useDispatch();
    const classes = useStyle();


    const toggleDrawer = (open) => (event) => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }
        dispatch(setDrawerState(open));
    };

    return (
        <Drawer
            className="slidebar"
            anchor="left"
            open={openDrawer}
            onClose={toggleDrawer(false)}
        >
            <CloseIcon
                className={classes.close_btn}
                onClick={toggleDrawer(false)} />
            <Searchbar mobile={true} />
            <Admin style={{
                display: "flex",
                flexDirection: "row",
                padding: "10px 10px",
                margin: "40px 34px"
            }} />
            <CustomerSupport style={{
                margin: "0 auto",
            }} />
            <MobileNavItems />
        </Drawer>
    )
}

export default SlideBar; 
