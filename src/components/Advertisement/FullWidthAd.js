import React from 'react';
import './FullWidthAd.css';

const FullWidthAd = props => {
    return (
        <div className="full_width_ad_container">
            <div>
                <div className="promotion_detail">
                    <p style={{ color: "green" }}>Father's Day Deal!</p>
                    <h1>SALE 50% OFF</h1>
                    <h3>ALL VEGETABES PRODUCTS</h3>
                </div>
            </div>
        </div>
    );
};

export default FullWidthAd;