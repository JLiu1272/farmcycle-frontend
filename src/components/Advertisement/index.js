import React from 'react';
import './Advertisement.css';

const ads = [
    {
        "img_link": require("../../assets/img/advertisements/banner1.png"),
        "title": "Fresh Vegetables",
        "link": "shop"
    },
    {
        "img_link": require("../../assets/img/advertisements/banner1.png"),
        "title": "Natural Fresh Fruit",
        "link": "shop"
    },
];

const Advertisement = props => {
    return (
        <div className="ad_container">
            {
                ads.map((ad, index) => {
                    if (index < props.numAds) {
                        return (
                            <div
                                key={ad.title + index}
                                style={{ backgroundImage: `url(${ad.img_link}` }}
                            >
                                <p>
                                    {ad.title}
                                    <br />
                                    <a href={`/${ad.link}`}>SHOP NOW</a>
                                </p>
                            </div>
                        );
                    }
                    return null;
                })
            }
        </div>
    );
};

export default Advertisement;