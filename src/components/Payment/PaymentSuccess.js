import React from 'react';
import { ThemeProvider } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import ShoppingBasket from '@material-ui/icons/ShoppingBasket';

// Mixin
import { pushToHistory } from '../Common/UtilityFunc';

// Constants
import theme from '../../constants/theme'
import * as ROUTES from '../../constants/routes';

const PaymentSuccess = () => {
    return (
        <ThemeProvider theme={theme}>
            <Box component="center" p={5}>
                <ShoppingBasket style={{ fontSize: "15rem" }} color="primary" />
                <Typography variant="h3">
                    Thank you for ordering from Farm Cycle
                </Typography>
                <Box p={4}>
                    We appreciate your business, and hope you will order from us again!
                    <br />
                    If you have any questions, please email us at
                    <br />
                    <a href="mailto:orders@farmcycle.us">orders@farmcycle.us</a>
                    <Box m={2}>
                        <Button
                            color="primary"
                            variant="contained"
                            onClick={() => pushToHistory(ROUTES.HOME)}>
                            Back to Shop
                        </Button>
                    </Box>
                </Box>
                <Divider />
            </Box>
        </ThemeProvider>
    );
};

export default PaymentSuccess;