import { makeStyles } from '@material-ui/core/styles';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

const AlertMsg = (props) => {
    const useStyles = makeStyles(() => ({
        status_msg: {
            color: `${props.color ? props.color : "green"}`,
            display: "flex",
        },
        space: {
            marginLeft: 10,
        }
    }))

    const classes = useStyles();
    const [showAlert, setShowAlert] = useState(true);

    const getShowAlert = () => {
        setTimeout(() => {
            setShowAlert(false);
        }, 10000);
    }

    useEffect(() => {
        if (props.addTimer) { getShowAlert(); }
    }, [props.addTimer]);

    return (
        <div>
            {
                showAlert &&
                <div className={classes.status_msg}>
                    {
                        props.icon &&
                        <span mt={2}>{props.icon}</span>
                    }
                    <span className={classes.space}>{props.msg}</span>
                </div>
            }
        </div>
    );
}

AlertMsg.propTypes = {
    color: PropTypes.string,
    icon: PropTypes.node,
    msg: PropTypes.string.isRequired,
    addTimer: PropTypes.bool.isRequired,
};

export default AlertMsg;