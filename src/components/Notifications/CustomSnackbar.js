/**
 * Notification message that times out or is closeable 
 */

import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

// Actions 
import { setSnackbarState } from '../../actions/State';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
        display: "flex",
        justifyContent: "center",
    },
}));

const CustomSnackbar = props => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        // When the snackbar closes due to timeout 
        // we must make sure to reset the message's state 
        if (props.setMsg) {
            props.setMsg({
                errorMsg: null,
                successMsg: null
            });
        } else {
            dispatch(setSnackbarState(null));
        }
    };

    return (
        <div className={classes.root}>
            <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                open={props.msg ? true : false}
                autoHideDuration={props.duration}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity={props.severity}>
                    {props.msg}
                </Alert>
            </Snackbar>
        </div>
    );
}


CustomSnackbar.propTypes = {
    severity: PropTypes.string.isRequired,
    msg: PropTypes.string,
};

export default CustomSnackbar; 