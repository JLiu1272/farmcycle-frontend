import React, { useState, useEffect } from 'react'
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Rating from '@material-ui/lab/Rating';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import LocationOn from '@material-ui/icons/LocationOn';
import { Avatar } from '@material-ui/core';
import { useParams } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';

// Components 
import TabPanel from './TabPanel'
import { farmerInfoByIdReq } from '../../mixin/ApiCaller';

// Styling 
import theme from '../../constants/theme'
import ProductToRow from '../Common/ProductToRow';

const useStyles = makeStyles((theme) => ({
    brand_header: {
        backgroundImage: `url("https://jegxdwep1p-flywheel.netdna-ssl.com/wp-content/uploads/2016/09/tumblr_o05v3eZmyT1ugn1wu_og_1280-1080x675.png")`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundSize: "cover",
        height: 400,
    },
    brand_logo: {
        backgroundImage: `url("https://i.pinimg.com/564x/a9/81/44/a98144654e29423ca0b4bf984d547b84.jpg")`,
        backgroundRepeat: "no-repeat",
        backgroundPosition: "center",
        backgroundSize: "cover",
        height: 200,
        width: 200,
    },
    categories_root: {
        '& > *': {
            margin: theme.spacing(0.5),
        }
    }
}))

function a11yProps(index) {
    return {
        id: `scrollable-auto-tab-${index}`,
        'aria-controls': `scrollable-auto-tabpanel-${index}`,
    };
}


const FarmerBio = props => {
    const classes = useStyles()
    const { farmerId } = useParams()
    const [farmerInfo, setFarmerInfo] = useState({})

    useEffect(() => {
        farmerInfoByIdReq(farmerId).then(res => {
            if (res !== "Failed to fetch") {
                setFarmerInfo(res)
            }
        })
    }, [farmerId])

    const SalesRepContact = () => {
        return <Box p={4}>
            <p>Sales Representative</p>
            <center>
                <Avatar src="https://i.etsystatic.com/iusa/f55ca3/57173233/iusa_75x75.57173233_m2tc.jpg?version=0" alt="Kylee" />
            </center>
            <Box component="center" mt={2}>
                {farmerInfo.contactPerson}
            </Box>
            <center>
                <Button color="primary" href={`tel:+1${farmerInfo.phoneNumber}`}>Contact</Button>
            </center>
        </Box>
    }

    const FarmCategories = props => {
        return (
            <Box component="center" m={5}>
                <Typography variant="h5">
                    Farm Categories
                </Typography>
                <Box
                    className={classes.categories_root}
                    display="flex"
                    justifyContent="center"
                    m={2}
                    flexWrap="wrap">
                    {farmerInfo.tags.map((category, index) => {
                        return (
                            <Chip
                                key={`${category}-${index}`}
                                label={category}
                            />
                        )
                    })}
                </Box>
            </Box>
        )

    }

    const FarmTags = props => {
        const [value, setValue] = useState(0)

        const handleChange = (event, newValue) => {
            setValue(newValue)
        }

        return (
            <Paper className={classes.root}>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                >
                    <Tab label="Description" {...a11yProps(0)} />
                    <Tab label="Available Products" {...a11yProps(1)} />
                </Tabs>
                <TabPanel value={value} index={0}>
                    <center>
                        {farmerInfo.description}
                    </center>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <ProductToRow numCols={4} productsArr={farmerInfo.products} />
                </TabPanel>
            </Paper>
        )
    }

    const FarmSummary = props => {
        return (
            <div style={{ width: '100%' }}>
                <Box display="flex" flexWrap="wrap">
                    <Box component="div" m={2} className={classes.brand_logo}></Box>
                    <Box flexGrow={1} component="div" m={4} pb={2}>
                        <Typography component="h5" variant="h5">
                            {farmerInfo.farmName}
                        </Typography>
                        <p>{farmerInfo.slogan}</p>
                        <Box display="flex" component="span">
                            <Box pr={1} ml={-1}>
                                <LocationOn />
                                {farmerInfo.city}, {farmerInfo.state}
                            </Box>
                            <Divider orientation="vertical" flexItem />
                            <Box pl={2}>
                                On FarmCycle since {new Date(farmerInfo.registryDate).getFullYear()}
                            </Box>
                        </Box>
                        <Box display="flex" mt={2}>
                            <Box pr={1}>
                                {farmerInfo.numSales} Sales
                        </Box>
                            <Divider orientation="vertical" flexItem />
                            <Box pl={2}>
                                <Rating
                                    value={farmerInfo.rating}
                                    precision={0.5}
                                    max={5}
                                    name={`hover - effect - farmer`}
                                />
                            </Box>
                        </Box>
                    </Box>
                    <SalesRepContact />
                </Box>
            </div>
        )
    }

    return (
        <ThemeProvider theme={theme}>
            <div>
                {(Object.keys(farmerInfo).length > 2) &&
                    < Box component="span">
                        <div className={classes.brand_header}></div>
                        <FarmSummary />
                        <FarmCategories />
                        <FarmTags />
                    </Box>
                }
                {
                    (Object.keys(farmerInfo).length === 2) &&
                    <Box component="center" m={12}>
                        Farmer Does Not Exist
                </Box>
                }
                {
                    (Object.keys(farmerInfo).length < 2) &&
                    <Box component="center" m={12}>
                        Server is unavailable
                </Box>
                }
            </div>
        </ThemeProvider>

    );
};


export default FarmerBio;