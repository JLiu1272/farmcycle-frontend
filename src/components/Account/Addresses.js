import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles, ThemeProvider, TextField, DialogTitle, DialogContent, DialogActions } from '@material-ui/core/';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Button from '@material-ui/core/Button';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import HomeWorkOutlined from '@material-ui/icons/HomeWorkOutlined';
import DeleteIcon from '@material-ui/icons/Delete';
import Add from '@material-ui/icons/Add';

// Components 
import TransitionDialog from '../Common/TransitionDialog'
import CustomSnackbar from '../Notifications/CustomSnackbar'

// Mixins 
import { camelCase, addSpaces } from '../Common/UtilityFunc'

// Actions 
import { setSnackbarState } from '../../actions/State';
import { addShippingAddress, removeShippingAddress, setShippingAddress } from '../../actions/User'

// API 
import { addShippingAddressReq, shippingAddressesReq, removeShippingAddressReq } from '../../mixin/ApiCaller'

// Constants 
import theme from '../../constants/theme'

const useStyle = makeStyles((theme) => ({
    address_title: {
        margin: theme.spacing(4, 0, 2)
    },
    address_root: {
        backgroundColor: theme.palette.background.paper,
    },
    single_address: {
        paddingRight: 30,
    },
    form_root: {
        minWidth: 450
    }
}))

const Address = (props) => {
    const classes = useStyle()
    const dispatch = useDispatch()

    const { addressLine1, zipcode } = props.addressInfo

    const deleteShippingAddress = (addressLine1) => {
        removeShippingAddressReq(addressLine1).then(response => {
            let severity = "success"
            let msg = ""
            // If the response is a string, there was an issue 
            // during the fetch process
            if (typeof response === "string") {
                msg = response
                severity = "error"
            }
            else {
                msg = response.Message
                dispatch(removeShippingAddress(addressLine1))
            }
            dispatch(setSnackbarState({
                severity: severity,
                msg: msg
            }));
        })
    }

    return <List>
        <ListItem
            classes={{
                container: classes.single_address,
            }}
            divider={true}
        >
            <ListItemAvatar>
                <Avatar>
                    <HomeWorkOutlined />
                </Avatar>
            </ListItemAvatar>
            <ListItemText
                primary={addressLine1}
                secondary={zipcode}
            />
            <ListItemSecondaryAction>
                <IconButton edge="end" aria-label="delete" onClick={() => deleteShippingAddress(addressLine1)}>
                    <DeleteIcon />
                </IconButton>
            </ListItemSecondaryAction>
        </ListItem>
    </List >
}

const AddAddressForm = (props) => {
    const classes = useStyle();
    const dispatch = useDispatch()
    const marginSize = 3
    const formFields = [
        {
            "name": "address_line_1",
            "type": "text",
            "textField": true,
            "required": true,
            "error": false,
        },
        {
            "name": "address_line_2",
            "type": "text",
            "textField": true,
            "required": false,
            "error": false,
        },
        {
            "name": "business_name",
            "type": "text",
            "textField": true,
            "required": false,
            "error": false,
        },
        {
            "name": "zip_code",
            "type": "number",
            "textField": false,
            "required": true,
            "error": false,
        },
        {
            "name": "delivery_instruction",
            "type": "text",
            "textField": false,
            "required": false,
            "error": false,
        },
    ]

    const [address, setAddress] = useState(
        _.reduce(formFields, (result, field) => {
            result[camelCase(field.name)] = {
                value: "",
                error: false
            }
            return result
        }, {})
    );
    const [msg, setMsg] = useState({
        errorMsg: null,
        successMsg: null,
    });
    const [disableBtn, setDisableBtn] = useState(null)

    const handleChange = (prop) => (event) => {
        setAddress({
            ...address, [prop]: {
                value: event.target.value,
                error: false
            }
        });
    };

    // Loops through all the fields, and check if 
    // there values are emptied. If they are, see if 
    // this field is required. If it is empty and is required, 
    // we know there is an error 
    const isValidForm = useCallback(() => {
        let formHasError = false
        _.forEach(formFields, (field) => {
            const key = camelCase(field.name)
            const { value } = address[key]
            if (value === "" && field.required) {
                address[key].error = true
                formHasError = true
            }
        })
        setAddress(address)
        // If form has error, it means the form is invalid. 
        // If has form does not have error, it means it is valid 
        return !formHasError
    }, [address, formFields])

    useEffect(() => {
        // If form is valid, we set disable button to false 
        // otherwise, we disable the button 
        setDisableBtn(!isValidForm())
    }, [address, disableBtn, isValidForm])

    const zipCodeToInt = () => {
        // Need to cast zip code to an integer 
        // Make sure that zipCode is not an empty string
        if (address["zipCode"] !== "") {
            address["zipCode"] = {
                ...address["zipCode"],
                value: parseInt(address["zipCode"].value, 10),
            }
        }
    }

    const submitForm = () => {
        // If there is error in this form, 
        // do not make API request
        if (!isValidForm()) {
            return
        }

        zipCodeToInt()
        // Grab the values from the address, and ignore the error field 
        const addressValues = _.reduce(address, (result, addressDetail, key) => {
            result[key] = addressDetail.value
            return result
        }, {})
        addressValues["isDefault"] = true

        addShippingAddressReq(addressValues).then(response => {
            // If the response came back as true, 
            // it means the operation was successful, otherwise
            // it failed
            if (typeof response === "string") {
                setMsg({ ...msg, errorMsg: "We had an issue adding address" })
            } else {
                dispatch(addShippingAddress(addressValues))
                setMsg({ ...msg, successMsg: response.Message })
            }
        })
    }

    return (
        <div>
            <CustomSnackbar
                duration={6000}
                severity={msg.errorMsg ? "error" : "success"}
                setMsg={setMsg}
                msg={msg.errorMsg ? msg.errorMsg : msg.successMsg}
            />
            <form className={classes.form_root}>
                <DialogTitle id="form-title">Add an address</DialogTitle>
                <DialogContent dividers={true}>
                    {formFields.map((field) => {
                        if (field.textField) {
                            return (
                                <Box m={marginSize} key={`${field.name}-root`}>
                                    <TextField
                                        id={`textfield-${field.name}`}
                                        type={field.type}
                                        label={addSpaces(field.name)}
                                        value={address[camelCase(field.name)].value}
                                        onChange={handleChange(camelCase(field.name))}
                                        fullWidth={true}
                                        error={address[camelCase(field.name)].error}
                                        required={field.required}
                                    />
                                </Box>
                            )
                        }
                        return null
                    })}
                    <Box m={marginSize}>
                        <TextField
                            id="textfield-zipcode"
                            type="number"
                            label="Zip Code"
                            required={true}
                            value={address["zipCode"].value}
                            error={address["zipCode"].error}
                            onChange={handleChange("zipCode")}
                        />
                    </Box>
                    <Box m={marginSize}>
                        <TextField
                            id="delivery-instr-multiline"
                            label="Instructions for delivery (optional)"
                            value={address["deliveryInstruction"].value}
                            multiline
                            rows={5}
                            required={false}
                            fullWidth={true}
                            variant="outlined"
                            error={address["deliveryInstruction"].error}
                            onChange={handleChange("deliveryInstruction")}
                        />
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={disableBtn}
                        aria-label="Add address"
                        onClick={submitForm}
                        startIcon={<Add />}
                    >
                        Address
                </Button>
                </DialogActions>
            </form>
        </div>
    )
}

const Addresses = (props) => {
    const classes = useStyle()
    const dispatch = useDispatch()
    const { addresses } = useSelector(state => state.user)

    useEffect(() => {
        if (addresses === null) {
            shippingAddressesReq().then(addresses => {
                if (typeof addresses === "string") {
                    dispatch(setSnackbarState({
                        severity: "error",
                        msg: addresses
                    }));
                } else {
                    dispatch(setShippingAddress(addresses))
                }
            })
        }
    }, [addresses, dispatch])

    return (
        <Box m={2}>
            <ThemeProvider theme={theme}>
                <div>
                    <Typography variant="h6" className={classes.addresses_title}>
                        Addresses
                    </Typography>
                    <div className={classes.address_root}>
                        {_.map(addresses, (value, key) => {
                            return <Address addressInfo={value} key={key} />
                        })}
                    </div>
                    <TransitionDialog
                        icon={<Add />}
                        content={<AddAddressForm />}
                        btnLabel="Address"
                    />
                </div>
            </ThemeProvider>
        </Box>
    )
}

export default Addresses