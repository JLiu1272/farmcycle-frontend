import React, { useState, useEffect } from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles, ThemeProvider } from '@material-ui/core/';
import ListAltIcon from '@material-ui/icons/ListAlt';
import PaymentIcon from '@material-ui/icons/Payment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import Home from '@material-ui/icons/Home';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import theme from '../../constants/theme';
import { useDispatch, useSelector } from 'react-redux';

// Custom Components 
import Banner from '../Common/Banner';
import BasicForm from '../Common/BasicForm';
import Addresses from './Addresses';

// Functions 
import { nullToStr } from '../Common/UtilityFunc';
import { withAuthentication } from '../Session';
import { updateUserInfo, setAccountActiveTab } from '../../actions/User'
import { updatePhoneNumberReq } from '../../mixin/ApiCaller'

const useStyle = makeStyles((theme) => ({
    root: {
        display: "flex",
        padding: "5rem"
    },
    tab_root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
        display: 'flex',
        minHeight: 400,
    },
    menu_actions_btn: {
        fontSize: "1.5rem",
        fontWeight: 550,
    },
    tab_divider: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
    form_root: {
        width: 600,
        display: "block",
    }
}))

const TabPanel = (props) => {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    {children}
                </Box>
            )}
        </div>
    );
}

const tabExtraProps = (index) => {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    }
}

/**
 * A page where use can edit their profile
 * @param {*} props 
 */
const Profile = withAuthentication(props => {
    const classes = useStyle();
    const dispatch = useDispatch()
    const [msg, setMsg] = useState({
        errorMsg: null,
        successMsg: null,
    });

    const formFields = [
        {
            "name": "display_name",
            "type": "text",
        },
        {
            "name": "email",
            "type": "text",
        },
        {
            "name": "phone_number",
            "type": "number",
        },
    ]

    const { displayName, email, phoneNumber } = useSelector(state => state.user)
    const [values, setValues] = useState(null);

    useEffect(() => {
        setValues({
            "displayName": nullToStr(displayName),
            "email": nullToStr(email),
            "phoneNumber": nullToStr(phoneNumber)
        })
    }, [displayName, email, phoneNumber])

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
    };

    const submitForm = () => {
        Promise.all([
            props.firebase.updateProfile({
                displayName: values.displayName
            }),
            props.firebase.updateEmail(values.email),
            updatePhoneNumberReq(values.phoneNumber)
        ]).then(() => {
            dispatch(updateUserInfo({
                "displayName": values.displayName,
                "email": values.email,
                "phoneNumber": values.phoneNumber,
            }))
            setMsg({
                errorMsg: null,
                successMsg: "Profile updated successfully"
            })
        }, (error) => {
            setMsg({
                errorMsg: error.message,
                successMsg: null
            })
        })
    }

    return (
        <form className={classes.form_root}>
            {values &&
                <BasicForm
                    formFields={formFields}
                    values={values}
                    setMsg={setMsg}
                    msg={msg}
                    handleChange={handleChange}
                    submitForm={submitForm}
                    actionLabel="Update"
                />
            }
        </form>
    )
})

const AccountMenu = props => {
    const classes = useStyle();
    const dispatch = useDispatch()
    const { accountActiveTab } = useSelector(state => state.user)

    const menuItems = [
        {
            "title": "profile",
            "icon": <AccountCircleIcon />
        },
        {
            "title": "Addresses",
            "icon": <Home />
        },
        {
            "title": "payment",
            "icon": <PaymentIcon />
        },
        {
            "title": "your orders",
            "icon": <ListAltIcon />
        },
    ];

    const handleChange = (event, newTab) => {
        dispatch(setAccountActiveTab(newTab))
    };

    return (
        <div className={classes.tab_root}>
            <ThemeProvider theme={theme}>
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    onChange={handleChange}
                    value={accountActiveTab}
                    textColor="primary"
                    indicatorColor="primary"
                    className={classes.tab_divider}
                    aria-label="account menu bar">
                    {menuItems.map((item, index) => {
                        return (
                            <Tab key={`${item}-${index}`} label={item.title} icon={item.icon} {...tabExtraProps(index)}>
                            </Tab>
                        )
                    })}
                </Tabs>
            </ThemeProvider>
            <TabPanel value={accountActiveTab} index={0}>
                <Profile />
            </TabPanel>
            <TabPanel value={accountActiveTab} index={1}>
                <Addresses />
            </TabPanel>
        </div>
    )
}

const Account = props => {
    const classes = useStyle();
    const { displayName } = useSelector(state => state.user)

    return (
        <div>
            <Banner title={`Hi ${displayName}!`} />
            <Container maxWidth="md" className={classes.root}>
                <AccountMenu />
            </Container>
        </div>
    );
};

export default withAuthentication(Account);