import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import './Values.css';

const values = [
    {
        "icon": "plane-departure",
        "title": "Free Shipping",
        "content": "Free shipping on all US order or order above $200"
    },
    {
        "icon": "plane-departure",
        "title": "Free Shipping",
        "content": "Free shipping on all US order or order above $200"
    },
    {
        "icon": "plane-departure",
        "title": "Free Shipping",
        "content": "Free shipping on all US order or order above $200"
    },
    {
        "icon": "plane-departure",
        "title": "Free Shipping",
        "content": "Free shipping on all US order or order above $200"
    },
];

const ValueItems = (props) => {
    return (
        <div className="value_unit">
            <FontAwesomeIcon
                className="values_icon"
                icon={props.icon} />
            <div>
                <span className="value_title">{props.title}</span>
                <br />
                <span>{props.content}</span>
            </div>
        </div>
    );
}

class Values extends Component {
    render() {
        return (
            <div className="values_container">
                {values.map((value, index) =>
                    <ValueItems
                        key={index}
                        icon={value.icon}
                        title={value.title}
                        content={value.content}
                    />
                )}
            </div>
        );
    }
}

export default Values;