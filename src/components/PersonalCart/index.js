/**
 * Personal Cart from user 
 */

import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CardMedia from '@material-ui/core/CardMedia';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import _ from 'lodash';
import { useDispatch, useSelector } from 'react-redux';

import { removeFromCart, renderCartItems } from '../../actions/Cart';
import { setSellerNote } from '../../actions/User'
import { setSnackbarState } from '../../actions/State';
import { withAuthentication } from '../Session';
import {
    totalPrice,
    toPriceStr,
    makePayment,
    restructureCartItems
} from '../../mixin/PaymentFunc';
import {
    productDetailsInCartReq,
    removeFromCartReq,
    confirmCheckoutReq
} from '../../mixin/ApiCaller';
import theme from '../../constants/theme';

// Components
import Banner from '../Common/Banner';
import AdjustQuantity from '../Product/AdjustQuantity';
import Delivery from '../Delivery/'


// CSS
import '../Product/Product.css';

const StyledTableCell = withStyles((theme) => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 18,
    },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
    root: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.action.hover,
        },
    },
}))(TableRow);

const useStyles = makeStyles({
    table: {
        width: "87%",
        margin: 84,
    },
    cover: {
        minWidth: "11rem",
        minHeight: 171,
    },
    close_btn: {
        cursor: "pointer"
    },
    row_header: {
        fontSize: 18,
    },
    special_instr_root: {
        width: "19rem",
        border: "1px solid"
    },
    cc_payment_icon: {
        transform: "translateX(-48px)",
    }
});

const CartHeader = props => {
    const classes = useStyles();

    return (
        <TableHead>
            <TableRow>
                {props.headers.map((header, index) => {
                    return (
                        <StyledTableCell
                            className={classes.row_header}
                            key={`${header}-${index}`}>
                            {header}
                        </StyledTableCell>
                    )
                })}
            </TableRow>
        </TableHead>
    )
}

const CartBody = props => {
    const classes = useStyles();
    const { cart } = useSelector(state => state);
    const dispatch = useDispatch();

    // If thumbnail 
    const LoadThumbnail = (props) => {
        if (props.thumbnail) {
            return <CardMedia
                className={classes.cover}
                image={props.thumbnail}
                src={props.thumbnail}
                title={`${props.itemName}-thumbnail`}
            />
        }
        return <CircularProgress />
    }

    return (
        <TableBody>
            {Object.keys(cart.cartItems).map((id) => {
                const cartItem = cart.cartItems[id];
                const quantity = cartItem.quantity;

                return (
                    <StyledTableRow key={`${cartItem.itemName}-${id}`}>
                        <StyledTableCell component="th" scope="row">
                            <LoadThumbnail thumbnail={cartItem.thumbnail} itemName={cartItem.itemName} />
                        </StyledTableCell>
                        <StyledTableCell align="left">
                            {cartItem.itemName}
                        </StyledTableCell>
                        <StyledTableCell align="left">
                            {cartItem.farmerName}
                        </StyledTableCell>
                        <StyledTableCell align="left" style={{ color: "green" }}>${cartItem.price}</StyledTableCell>
                        <StyledTableCell align="center">
                            <AdjustQuantity searchView={false} product_id={id} width="9rem" />
                        </StyledTableCell>
                        <StyledTableCell align="center">
                            <CloseIcon className={classes.close_btn} onClick={() => {
                                dispatch(removeFromCart(id, quantity));
                                removeFromCartReq(id, quantity);
                            }} />
                        </StyledTableCell>
                    </StyledTableRow>
                )
            })}
        </TableBody>
    )
}

const CartTable = props => {
    const headers = [
        "Image",
        "Product",
        "Seller",
        "Price",
        "Quantity",
        "Remove"
    ];

    return (
        <Box pl={2} pr={2} width="75%">
            <TableContainer>
                <Table aria-label="shopping-cart-table">
                    <CartHeader headers={headers} />
                    <CartBody />
                </Table>
            </TableContainer>
        </Box>

    );
}

const SpecialInstr = props => {
    const dispatch = useDispatch()

    const handleChange = (event) => {
        dispatch(setSellerNote(event.target.value))
    }

    return (
        <Box mt={2}>
            <TextField
                id="special-instruction-standard-textarea"
                rows={3}
                onChange={handleChange}
                fullWidth={true}
                label="Note to Seller"
                placeholder="Note"
                multiline
                variant="outlined"
            />
        </Box>
    )
}

const AcceptedPayments = () => {
    const classes = useStyles();

    return (
        <Box p={1}>
            <FormLabel component="legend">Accepted Payments</FormLabel>
            <img src={require('../../assets/payment/cc-badges.png')} alt="Pay with Credit Card" className={classes.cc_payment_icon} />
        </Box>
    )
}

const Checkout = withAuthentication((props) => {
    const classes = useStyles();
    const dispatch = useDispatch()

    const { size, cartItems } = useSelector(state => state.cart);
    const {
        displayName,
        phoneNumber,
        deliveryAddr,
        deliveryOption,
        deliveryTime,
        addresses,
        sellerNote
    } = useSelector(state => state.user)
    const [cartTotal, setCartTotal] = useState(totalPrice(cartItems));
    const discount = 0;
    const shipping = 0;

    useEffect(() => {
        setCartTotal(totalPrice(cartItems));
    }, [cartItems])

    const paymentBreakdown = [
        {
            name: "Item(s) total",
            divider: false,
            fontWeight: "normal",
            isNegative: false,
            value: cartTotal,
        },
        {
            name: "Discount",
            divider: true,
            fontWeight: "normal",
            isNegative: true,
            value: discount,
        },
        {
            name: "Subtotal",
            divider: false,
            fontWeight: "bold",
            isNegative: false,
            value: cartTotal - discount,
        },
        {
            name: "Delivery Fee",
            divider: true,
            fontWeight: "normal",
            isNegative: false,
            value: shipping,
        },
        {
            name: `Total (${size} items)`,
            divider: false,
            fontWeight: "bold",
            isNegative: false,
            value: cartTotal - discount + shipping
        },
    ];


    const handlePayment = () => {
        const receiptContent = {
            "name": displayName,
            "deliveryOption": deliveryOption,
            "deliveryAddress": deliveryOption === "pickup" ? deliveryAddr : addresses[deliveryAddr],
            "customerID": props.authUser.uid,
            "phoneNumber": phoneNumber,
            "desiredArrivalTime": deliveryTime,
            "sellerNote": sellerNote,
            "cart": restructureCartItems(cartItems, ["itemName", "quantity", "price"])
        }
        confirmCheckoutReq(receiptContent)
            .then(res => {
                if (typeof res === "string") {
                    dispatch(setSnackbarState({
                        severity: "error",
                        msg: res
                    }));
                } else {
                    makePayment(cartItems, "usd", ["itemName", "price", "quantity"])
                }
            })
            .catch(err => {
                dispatch(setSnackbarState({
                    severity: "error",
                    msg: err.message
                }));
            })
    }

    return (
        <ThemeProvider theme={theme}>
            <Box pl={2}>
                <Card raised classes={{ root: classes.special_instr_root }}>
                    <CardContent>
                        <AcceptedPayments />
                        {paymentBreakdown.map((category) => {
                            return (
                                <div key={`${category.name}`}>
                                    <Box display="flex" >
                                        <Box p={1} flexGrow={1} fontWeight={category.fontWeight}>
                                            {category.name}
                                        </Box>
                                        <Box p={1}>
                                            {toPriceStr(category.isNegative, (category.value).toFixed(2))}
                                        </Box>
                                    </Box>
                                    {category.subtitle &&
                                        <Box pl={1} fontWeight={200}>
                                            {category.subtitle}
                                        </Box>
                                    }
                                    {category.divider && <Divider />}
                                </div>
                            )
                        })}
                        <SpecialInstr />
                        <Box p={2}>
                            <Grid container justify="center">
                                <Button
                                    onClick={handlePayment}
                                    variant="contained"
                                    disableElevation
                                    color="primary">
                                    Proceed to checkout
                                </Button>
                            </Grid>
                        </Box>
                    </CardContent>
                </Card>
            </Box >
        </ThemeProvider>
    )
})

const PersonalCart = props => {
    const [cartEmpty, setCartEmpty] = useState(false);

    const { size } = useSelector(state => state.cart);
    const dispatch = useDispatch();

    useEffect(() => {
        let didCancel = false;
        productDetailsInCartReq().then(cartItems => {
            if (!didCancel) {
                dispatch(renderCartItems(cartItems));
                setCartEmpty(_.isEmpty(cartItems));
            }
        })

        return () => {
            didCancel = true
        }

    }, [size, dispatch])

    return (
        <div>
            <Banner title="Your Shopping Cart" />
            <Delivery />
            {!cartEmpty &&
                <Box p={6} display="flex">
                    <CartTable />
                    <Checkout />
                </Box>
            }
            {cartEmpty &&
                <Box textAlign="center" m={5}>
                    <Typography variant="h4">
                        Cart is Empty
                    </Typography>
                </Box>
            }
        </div>
    );
};

export default PersonalCart;