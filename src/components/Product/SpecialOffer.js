import React from 'react';
import Badge from 'react-bootstrap/Badge';

const SpecialOffer = (props) => {
    return props.offer
        ? <Badge variant="success">{`${props.offer}% off`}</Badge>
        : null
}

export default SpecialOffer