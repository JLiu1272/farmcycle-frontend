import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';

// Components
import AlertMsg from '../Notifications/AlertMsg';

// Actions 
import { updateQuantity, setCartSize } from '../../actions/Cart';
import { updateQuantityReq, cartSizeReq } from '../../mixin/ApiCaller';

import './Product.css';
import { useSelector, useDispatch } from 'react-redux';

/**
 * A component for allowing users to change the number of 
 * X item they want.  
 * @param {} props 
 */
const AdjustQuantity = (props) => {
    const useStyles = makeStyles((theme) => ({
        root: {
            display: 'flex',
            border: "solid 1px green",
            width: `${props.width ? props.width : "100%"}`
        },
        input: {
            marginLeft: theme.spacing(1),
            flex: 1,
        },
        iconButton: {
            backgroundColor: "green",
            borderRadius: 0,
            "&:hover": {
                backgroundColor: "green",
                opacity: 0.5,
            }
        },
        icon: {
            color: "white",
        }
    }));

    const classes = useStyles();
    const { cartItems } = useSelector(state => state.cart)
    const productId = props.product_id;
    const [quantity, setQuantity] = useState(0);
    const [errorMsg, setErrorMsg] = useState(null);
    const dispatch = useDispatch();

    useEffect(() => {
        const initQuantity = !props.searchView && productId in cartItems ? cartItems[productId].quantity : 1;
        setQuantity(initQuantity);
    }, [cartItems, productId, props.searchView])

    const updateRefreshQuantity = async (new_quantity) => {
        await updateQuantityReq(productId, new_quantity)
        cartSizeReq().then(size => dispatch(setCartSize(size)))
    }

    const incrementQuantity = () => {
        const new_quantity = quantity + 1;
        setQuantity(new_quantity);
        props.searchView ? dispatch(updateQuantity(new_quantity)) : updateRefreshQuantity(new_quantity)
    }

    const decrementQuantity = () => {
        if (quantity - 1 > 0) {
            const new_quantity = quantity - 1;
            setQuantity(new_quantity);
            props.searchView ? dispatch(updateQuantity(new_quantity)) : updateRefreshQuantity(new_quantity)
        }
    }

    const handleInputChange = (event) => {
        const new_quantity = parseFloat(event.target.value);
        if (new_quantity <= 0 || isNaN(new_quantity)) {
            setErrorMsg("Quantity has to be great than or equal to 1");
        }
        else if (!Number.isInteger(new_quantity)) {
            setErrorMsg("Quantity cannot be decimal values");
        }
        else {
            setQuantity(new_quantity);
            props.searchView ? dispatch(updateQuantity(new_quantity)) : updateRefreshQuantity(new_quantity)
            setErrorMsg(null);
        }
    }

    return (
        <div>
            <Paper component="form" className={classes.root}>
                <IconButton className={classes.iconButton} aria-label="decrement-quantity" onClick={decrementQuantity}>
                    <FontAwesomeIcon icon="minus" className={classes.icon} />
                </IconButton>
                <InputBase
                    className={classes.input}
                    value={quantity || 0}
                    type="number"
                    onChange={handleInputChange}
                    inputProps={{
                        'aria-label': `quantity`,
                    }}
                />
                <IconButton className={classes.iconButton} aria-label="increment-quantity" onClick={incrementQuantity}>
                    <FontAwesomeIcon icon="plus" className={classes.icon} />
                </IconButton>
            </Paper>
            {errorMsg && <AlertMsg addTimer={false} msg={errorMsg} color="red" />}
        </div>
    );
}

export default AdjustQuantity;