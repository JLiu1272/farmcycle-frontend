import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import React from 'react';

// CSS
import './Product.css';

// Mixin 
import { calcSalePrice } from '../../mixin/PaymentFunc';

const Unit = (props) => {
    const unitStyles = makeStyles({
        unit_tag: {
            opacity: 0.7,
            fontSize: 13,
            marginTop: `${props.showSalePrice ? 0 : 12}px`
        },
    })

    const classes = unitStyles()

    return <p className={classes.unit_tag}>at $3.12/lbs</p>
}

const PriceTag = (props) => {
    const { price, offer, showSalePrice } = props

    const priceStyles = makeStyles((theme) => ({
        sale_tag: {
            color: "gray",
            fontWeight: 500
        },
        root: {
            display: `${showSalePrice ? 'block' : 'flex'}`,
            flexWrap: 'wrap',
        },
        price_root: {
            display: 'flex',
            flexWrap: 'wrap',
            '& > *': {
                margin: theme.spacing(0.5),
            },
        }
    }))

    const classes = priceStyles();

    return (
        <div className={classes.root}>
            <div className={classes.price_root}>
                <Typography variant="h6" className="price_tag">
                    ${calcSalePrice(price, offer)}
                </Typography>
                {(offer && showSalePrice) &&
                    <Typography variant="h6" className={classes.sale_tag}>
                        <strike>
                            ${price}
                        </strike>
                    </Typography>
                }
            </div>
            <Unit showSalePrice={showSalePrice} />
        </div>
    )
}

export default PriceTag