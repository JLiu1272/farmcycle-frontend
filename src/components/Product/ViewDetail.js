import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import OpenWithOutlined from '@material-ui/icons/OpenWithOutlined';

// Components
import ProductDetail from '../ProductDetail';

// CSS
import '../Menu/index.css';

// mixin 
import { pushToHistory } from '../Common/UtilityFunc';

const ViewDetail = (props) => {
    const useStyles = makeStyles((theme) => ({
        product_details: {
            lineHeight: 3,
        },
        item_name: {
            fontSize: 25,
            fontWeight: 600,
        },
        actions_root: {
            display: "flex"
        },
        icon_btn: {
            backgroundColor: "green",
            borderRadius: 3,
            color: "white",
            "&:hover": {
                backgroundColor: "green",
                opacity: 0.5,
            }
        },
        closeButton: {
            position: "absolute",
            right: 5,
            top: 5,
            color: theme.palette.grey[500],
        },
        expandButton: {
            position: "absolute",
            right: 40,
            top: 5,
            color: theme.palette.grey[500],
        },
    }))
    const classes = useStyles();
    const { product, onClose } = props

    return (
        <div>
            <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                <CloseIcon />
            </IconButton>
            <IconButton
                aria-label="close"
                className={classes.expandButton}
                onClick={() => pushToHistory(`/product/${product.productId}`)}>
                <OpenWithOutlined />
            </IconButton>
            <ProductDetail product={product} onClose={onClose} summary={true} />
        </div>
    )
}

export default ViewDetail;