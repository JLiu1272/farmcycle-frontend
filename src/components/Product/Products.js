import React, { useState, useEffect } from 'react';
import ItemsCarousel from 'react-items-carousel';
import { connect, useDispatch } from 'react-redux';
import _ from 'lodash';

// Components
import Product from './Product';

// API 
import { allProductsByTagReq } from '../../mixin/ApiCaller';

// Reducer Actions
import { setProducts } from '../../actions/Product'

// CSS
import './Products.css';

const productTabs = ["Vegetables", "Fruits", "Salads"];

const ProductTabs = (props) => {
    if (props.showTab) {
        return (
            <div className="product_tabs_container">
                {productTabs.map((tab, index) =>
                    <button className="product_tabs" key={index}>{tab}</button>
                )}
            </div>
        );
    }
    return null;
}

const Products = (props) => {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const [numCards, setNumCards] = useState(4);
    const chevronWidth = 40;

    const dispatch = useDispatch()

    // Call this the first time the page was loaded
    const useMountEffect = (adjustNumCardsFunc) => useEffect(adjustNumCardsFunc, []);

    const debouncedHandleResize = _.debounce(
        function handleResize() {
            let width = window.innerWidth;
            if (width <= 500) {
                setNumCards(1);
            }
            else if (width <= 752) {
                setNumCards(2);
            }
            else if (width <= 1043) {
                setNumCards(3);
            }
            else {
                setNumCards(4);
            }
        }, 300);

    useMountEffect(debouncedHandleResize);

    // Render products and cache it into state 
    useMountEffect(() => {
        allProductsByTagReq().then(products => {
            dispatch(setProducts(products))
        })
    });

    // Only resize if window's innerWidth changes 
    useEffect(() => {
        window.addEventListener('resize', debouncedHandleResize)

        // Cleanup event listener. Prevent memory leak in program 
        return _ => {
            window.removeEventListener('resize', debouncedHandleResize)
        }
    }, [debouncedHandleResize])

    return (
        <div>
            <div className="product_title">
                <p className="subtitle">{props.subtitle}</p>
                <p className="title">{props.title}</p>
            </div>
            <ProductTabs showTab={props.showTab} />
            <div className="products_container">
                <ItemsCarousel
                    requestToChangeActive={setActiveItemIndex}
                    activeItemIndex={activeItemIndex}
                    numberOfCards={numCards}
                    infiniteLoop={false}
                    firstAndLastGutter={true}
                    rightChevron={'>'}
                    leftChevron={'<'}
                    outsideChevron
                    chevronWidth={chevronWidth}
                >
                    {_.map(props.inventory.products, (product) => {
                        return (
                            <Product
                                key={`${props.title}-${product.productId}`}
                                product={product}
                                isTopDown={true} />
                        );
                    })}
                </ItemsCarousel>
            </div>
        </div>
    );
}

function mapStateToProps(state) {
    return {
        inventory: state.inventory
    }
}

export default connect(mapStateToProps)(Products);