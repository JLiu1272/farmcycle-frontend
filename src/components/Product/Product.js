import Box from '@material-ui/core/Box';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Rating from '@material-ui/lab/Rating';
import React, { useState } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Card from 'react-bootstrap/Card';

// Components
import Menu from '../Menu';
import PriceTag from './PriceTag';
import SpecialOffer from './SpecialOffer';

// CSS
import './Product.css';

const Product = (props) => {
    const thumbnailMaxDim = props.thumbDim ? props.thumbDim : 11;
    const menuWidth = thumbnailMaxDim - 1;

    const productUseStyles = makeStyles((theme) => ({
        root: {
            display: "flex",
            borderRadius: 0,
            boxShadow: "none",
            border: 'none',
            maxWidth: `${thumbnailMaxDim}rem`,
            margin: `${props.isTopDown ? '0 auto' : '0'}`,
            padding: "20px 0px 20px",
            flexDirection: `${props.isTopDown ? "column" : "row"}`,
        },
        content: {
            flex: '1 0 auto',
            position: "relative",
            padding: `${props.isTopDown ? "16px 0px 0px 0px" : 16}`,
        },
        thumbnail: {
            width: `${thumbnailMaxDim}rem`,
            height: `${thumbnailMaxDim}rem`,
        },
        thumbnail_container: {
            position: "relative"
        },
        rating_root: {
            width: 200,
            display: 'flex',
        },
        rating_detail: {
            marginBottom: 8,
        },
        sale_icon: {
            position: "absolute",
            right: "4%",
            top: "2%",
        },
        seller_info: {
            color: "#007BFF",
            cursor: "pointer",
            padding: "3",
            "&:hover": {
                textDecoration: "underline"
            }
        },
        availability_tag: {
            color: "brown",
            fontSize: 12,
            fontWeight: 300,
        }
    }));

    const classes = productUseStyles();
    const [isHovering, toggleHoverState] = useState(false);
    const [numStars, setNumStars] = useState(0);
    const [numRaters, setNumRaters] = useState(0);

    const handleMouseHover = () => {
        toggleHoverState(!isHovering);
    }

    const updateRating = (newRating) => {
        setNumStars(newRating);
        setNumRaters(prevState => prevState + 1);
    }

    return (
        <Card
            id={`product-${props.product.productId}`}
            className={classes.root}
            onMouseOver={handleMouseHover}
            onMouseOut={handleMouseHover}
        >
            <div className={classes.thumbnail_container}>
                <CardMedia
                    className={classes.thumbnail}
                    image={props.product.thumbnail}
                    title={`${props.product.itemName} - photo`}
                />
                <div className={classes.sale_icon}>
                    <SpecialOffer offer={props.product.offer} />
                </div>
                <ReactCSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={500}
                >
                    {(isHovering && props.isTopDown) &&
                        <Menu
                            MenuWidth={`${menuWidth}rem`}
                            product={props.product}
                        />
                    }
                </ReactCSSTransitionGroup>
            </div>
            <CardContent className={classes.content}>
                <Typography variant="h5" className="item_name">
                    {props.product.itemName}
                </Typography>
                <PriceTag price={props.product.price} offer={props.product.offer} showSalePrice={false} />
                <p className={classes.availability_tag}>{`Only ${props.product.availability} left`}</p>
                <div className={classes.rating_root}>
                    <Rating
                        value={numStars}
                        precision={0.5}
                        max={5}
                        name={`hover - effect - ${props.product.id} - product`}
                        onChange={(event, newValue) => updateRating(newValue)}
                    />
                    <Box className={classes.rating_detail} ml={1}><a href="/">({numRaters})</a></Box>
                </div>
                <ReactCSSTransitionGroup
                    transitionName="fade"
                    transitionEnterTimeout={500}
                    transitionLeaveTimeout={500}
                >
                    {(isHovering && props.isLeftRight) &&
                        <Menu
                            style={{ bottom: 24 }}
                            MenuWidth={`${menuWidth + 3}rem`}
                            product={props.product}
                        />}
                </ReactCSSTransitionGroup>
            </CardContent>
        </Card >
    );
}

export default Product;