import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Rating from '@material-ui/lab/Rating';
import Button from '@material-ui/core/Button';
import { Typography, Divider } from '@material-ui/core';
import PropTypes from 'prop-types';
import _ from 'lodash'

// Components
import AdjustQuantity from '../Product/AdjustQuantity';
import PriceTag from '../Product/PriceTag';
import SpecialOffer from '../Product/SpecialOffer';
import ErrorMsg from '../Common/ErrorMsg'

// Actions 
import { addToCart, resetTmpQuantity } from '../../actions/Cart';
import { setSnackbarState } from '../../actions/State';

// API
import { addToCartReq, productByIdReq } from '../../mixin/ApiCaller';

// CSS
import 'font-awesome/css/font-awesome.min.css';
import '../Product/Product.css';
import { pushToHistory } from '../Common/UtilityFunc';

const useStyles = makeStyles((theme) => ({
    item_name: {
        fontSize: 25,
        fontWeight: 600,
    },
    actions_root: {
        display: "flex"
    },
    icon_btn: {
        backgroundColor: "green",
        borderRadius: 3,
        color: "white",
        "&:hover": {
            backgroundColor: "green",
            opacity: 0.5,
        }
    },
    root: {
        display: "flex",
        flexDirection: "row",
        marginTop: 10,
        '& > *': {
            margin: theme.spacing(1),
        },
    },
    details: {
        display: "flex",
        flexDirection: "column",
    },
    thumbnail: {
        minHeight: 300,
        minWidth: '20rem',
        margin: 16,
    },
    product_details: {
        lineHeight: 2,
    },
    cart_action: {
        display: "flex",
        '& > *': {
            marginRight: theme.spacing(2),
            marginTop: theme.spacing(2),
        },
    },
    thumbnail_container: {
        position: "relative"
    },
    sale_icon: {
        position: "absolute",
        right: "7%",
        top: "5%",
    },
    farmerbio_link: {
        color: "blue",
        cursor: "pointer",
        "&:hover": {
            textDecoration: "underline",
        }
    }
}))

const FarmerRating = (props) => {
    const classes = useStyles()

    return <Box component="span">
        <div>
            <strong>Farmer</strong>: <Button
                className={classes.farmerbio_link}
                onClick={() => pushToHistory(`/farmer/${props.farmerId}`)} color="primary">
                {props.farmerName}
            </Button>
        </div>
        <Rating
            size="small"
            value={4}
            precision={0.5}
            max={5}
            name={`hover - effect - ${props.productId} - product`}
        />
        <Divider />
    </Box>
}

const Thumbnail = (props) => {
    const { product } = props
    const classes = useStyles()

    return (
        <div className={classes.thumbnail_container}>
            <CardMedia
                className={classes.thumbnail}
                image={product.thumbnail}
                title={`${product.itemName}-thumbnail`}
            />
            <div className={classes.sale_icon}>
                <SpecialOffer offer={product.offer} />
            </div>
        </div>
    )
}

const AddToCartButton = (props) => {
    const { productId } = props
    const classes = useStyles()
    const dispatch = useDispatch();
    const { tmp_quantity, cartItems } = useSelector(state => state.cart);

    const confirmAddToCart = (productId) => {
        const quantity = productId in cartItems
            ? cartItems[productId].quantity + tmp_quantity
            : tmp_quantity
        addToCartReq(productId, quantity);
        dispatch(addToCart(productId, quantity));
        dispatch(resetTmpQuantity())
        dispatch(setSnackbarState({
            severity: "success",
            msg: "Added to cart!"
        }));
        if (props.onClose) { props.onClose() }
    }

    return (
        <div className={classes.actions_root}>
            <AdjustQuantity searchView={true} product_id={productId} width="85%" />
            <Tooltip title="Add to cart">
                <IconButton
                    aria-label="close"
                    disableRipple={false}
                    size="medium"
                    onClick={() => confirmAddToCart(productId)}
                    className={classes.icon_btn}
                >
                    <AddShoppingCartIcon />
                </IconButton>
            </Tooltip>
        </div>
    )
}

const ProductDetail = (props) => {
    const { productId } = useParams();
    var { products } = useSelector(state => state.inventory)
    const [product, setProduct] = useState({})

    useEffect(() => {
        // If productId is present, which comes 
        // from the url, then check if the productId 
        // is in the store. If it is in the store, use the store product data
        // otherwise rerender it 
        if (productId) {
            if (productId in products) {
                setProduct(products[productId])
            } else {
                productByIdReq(productId).then(product => {
                    const newProduct = product === "Failed to fetch" ? {} : product
                    setProduct(newProduct)
                })
            }
        }
        else {
            setProduct(props.product)
        }
    }, [productId, products, props.product])

    const classes = useStyles();
    const { summary } = props

    return (
        <div>
            {(_.size(product) > 0) &&
                <Card className={classes.root}>
                    <Thumbnail product={product} />
                    <div className={classes.details}>
                        <CardContent>
                            <div className={classes.product_details}>
                                <FarmerRating
                                    productId={product.productId}
                                    farmerName={product.farmerName}
                                    farmerId={product.farmerId} />
                                <Typography className={classes.item_name}>
                                    {product.itemName}
                                </Typography>
                                <PriceTag price={product.price} offer={product.offer} showSalePrice={true} />
                                {!summary &&
                                    <Typography paragraph={true}>
                                        {product.description}
                                    </Typography>
                                }
                            </div>
                            <p className="availability_tag">{`Only ${product.availability} left`}</p>
                            <AddToCartButton productId={product.productId} />
                        </CardContent>
                    </div>
                </Card>
            }
            {(_.size(product) <= 0) &&
                <ErrorMsg msg="Product does not exist" />
            }
        </div>
    )
}


ProductDetail.propTypes = {
    product: PropTypes.object,
};

export default ProductDetail;