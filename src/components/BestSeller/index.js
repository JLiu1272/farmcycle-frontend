import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';

import Product from '../Product/Product';

// Routes 
import * as ROUTES from '../../constants/routes';

// CSS
import '../Product/Product.css';
import './BestSeller.css';
import '../../assets/common.css';

const link = require("../../assets/img/advertisements/banner5.png");

const BestSeller = props => {

    return (
        <div>
            <div className="product_title">
                <p className="subtitle">All times favorites</p>
                <p className="title">Best Seller</p>
            </div>
            {/** Left side advertisement */}
            <div className="best_seller_container">
                <div
                    className="best_seller_ad"
                    style={{ backgroundImage: `url(${link})` }}
                >
                    <div>
                        <p className="ad_slogan">
                            Natural <br />Food
                        </p>
                        <a className="shop_now_btn" href={ROUTES.SHOP}>SHOP NOW</a>
                    </div>
                </div>
                <div className="best_seller_listing">
                    {_.map(props.inventory.products, (product, product_id) => {
                        return <Product
                            key={`${product.itemName}-${product.productId}-${product.farmerName}-bestseller`}
                            product={product}
                            thumbDim={9}
                            isLeftRight={true} />
                    })}
                </div>
            </div>
        </div>
    );
};

function mapStateToProps(state) {
    return {
        inventory: state.inventory
    }
}

export default connect(mapStateToProps)(BestSeller);