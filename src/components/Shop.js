import React, { useState, useEffect } from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import Divider from '@material-ui/core/Divider';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import _ from 'lodash'

// Action
import { updateSearchword } from '../actions/Search';

// API 
import { productsByKeywordReq } from '../mixin/ApiCaller'

// Components
import Banner from './Common/Banner';
import ProductToRow from './Common/ProductToRow';
import ErrorMsg from './Common/ErrorMsg'

// Department categories 
const filterTypes = [
    {
        "title": "Food Type",
        id: 9102,
        "type": "tree",
    },
    {
        "title": "Farmer",
        id: 9103,
        "type": "checkbox"
    },
    {
        "title": "Tags",
        id: 9104,
        "type": "checkbox"
    }
];

const globalStyle = makeStyles(() => ({
    title: {
        display: "flex",
        justifyContent: "center",
        lineHeight: "200px",
        fontSize: 40
    },
    filter_root: {
        maxHeight: 800,
        padding: "1em 1.5em 1em 1.5em"
    },
    search_root: {
        width: 1100,
        padding: "2em",
    },
    search_result_title: {
        marginLeft: "2%",
        fontSize: 30
    },
    shop_body: {
        display: "flex"
    },
    category_label: {
        fontSize: 15,
    },
    filter_root_header: {
        fontSize: 20,
        fontWeight: 600,
    },
    checkmark_container: {
        display: "flex",
    }
}))

// Check mark view for search filtering 
// i.e <Check box> Value 
const CheckMarkFilter = (props) => {
    const [checked, setChecked] = useState(true);
    const handleChange = (event) => {
        setChecked(event.target.checked);
    };

    return (
        <FormControlLabel
            style={{ marginBottom: 0 }}
            control={<Checkbox checked={checked} onChange={handleChange} name={props.label} />}
            label={props.label}
        />
    );
}

/**
 * The filter bar for the shopping page   
 * @param {} props 
 */
const RecursiveTreeFilter = (props) => {

    const globalClasses = globalStyle();
    const categories = useSelector(state => state.filterCategory);
    const dispatch = useDispatch();

    const renderTree = (nodes) => {
        return (
            <TreeItem
                key={nodes.id}
                nodeId={nodes.id}
                label={nodes.name}
                onLabelClick={() => dispatch(updateSearchword(nodes.name))}
                classes={{
                    label: globalClasses.category_label
                }}
            >
                {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
            </TreeItem>
        )
    }

    const DisplayCategories = (props) => {
        let treeView = categories.map((category, index) => {
            return (
                <div key={`${category.name}-${index}`}>
                    {renderTree(category)}
                </div>
            )
        });
        let checkmarkView = categories.map((category, index) => {
            return <CheckMarkFilter
                key={`${category.name}-${index}`}
                label={category.name} />
        });
        return props.type === "tree" ? treeView : checkmarkView;
    }

    return (
        <div>
            {filterTypes.map((filterHeader, index) => {
                return (
                    <TreeView
                        key={`${filterHeader.title}-${index}`}
                        className={globalClasses.filter_root}
                        defaultCollapseIcon={<ExpandMoreIcon />}
                        defaultExpanded={['root']}
                        defaultExpandIcon={<ChevronRightIcon />}
                    >
                        <Typography className={globalClasses.filter_root_header}>
                            {filterHeader.title}
                        </Typography>
                        <DisplayCategories type={filterHeader.type} />
                    </TreeView>
                )
            })}
        </div>
    );
}

const SearchResult = (props) => {
    const [products, setProducts] = useState([])
    const searchword = useSelector(state => state.searchword);

    useEffect(() => {
        productsByKeywordReq(searchword).then(products => {
            // Reason it could possibly be a string is 
            // when the api detected an error 
            setProducts(typeof products === 'string' ? [] : products)
        })
    }, [searchword])

    const globalClasses = globalStyle();

    return (
        <div className={globalClasses.search_root}>
            <Typography className={globalClasses.search_result_title}>
                Showing results for {searchword}
            </Typography>
            {_.size(products) === 0 && <ErrorMsg msg={`No match for ${searchword}`} />}
            {_.size(products) > 0 &&
                <ProductToRow numCols={4} products={products} />
            }
        </div>
    )
}

const Shop = props => {
    const globalClasses = globalStyle();
    return (
        <div>
            <Banner title="Shop" />
            <div className={globalClasses.shop_body}>
                <RecursiveTreeFilter />
                <Divider orientation="vertical" flexItem />
                <SearchResult />
            </div>
            <Divider />
        </div>
    );
};

export default Shop;