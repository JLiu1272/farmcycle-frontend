import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Dialog from '@material-ui/core/Dialog';
import { useDispatch, useSelector } from 'react-redux';

// Components
import ViewDetail from '../Product/ViewDetail';

// Actions 
import { setSnackbarState } from '../../actions/State';
import { setCartSize } from '../../actions/Cart';
import {
    addToCartReq,
    removeFromWishlistReq,
    addToWishlistReq,
    isFavoritedByCustomerReq,
    cartSizeReq,
} from '../../mixin/ApiCaller';

import '../Menu/index.css';

const Menu = (props) => {
    // Styling purposes 
    const useStyles = makeStyles((theme) => ({
        menu_icon: {
            width: 30,
            textAlign: "center",
            verticalAlign: "middle",
            cursor: "pointer",
        },
        menu_width: {
            width: `${props.MenuWidth}`
        },
    }))

    const [isFavorited, setIsFavorited] = useState(false)
    const [activeMenu, setActiveMenu] = useState(null);
    const [open, setOpen] = useState(false);
    const { cartItems } = useSelector(state => state.cart)
    const productId = props.product.productId;
    const classes = useStyles();
    const dispatch = useDispatch();

    // The different menu action 
    // inside this menu tab 
    const tools = [
        {
            icon_name: "fa fa-shopping-cart",
            tooltip: "Add to card",
            alias: "shoppingCart",
            showDialog: false,
        },
        {
            icon_name: "fa fa-search",
            tooltip: "Search for similar items",
            alias: "viewDetails",
            showDialog: true,
        },
        {
            icon_name: "fa fa-heart",
            tooltip: `${isFavorited ? "Unfavorite" : "Favorite"}`,
            alias: "favorite",
            showDialog: false,
        },
    ];

    // Check for whether the product is favorited or not 
    // Only gets check whether user hover over a product 
    // to get the menu 
    useEffect(() => {
        isFavoritedByCustomerReq(productId).then(setIsFavorited);
    }, [productId])

    const updateCart = async () => {
        const quantity = productId in cartItems ? cartItems[productId].quantity + 1 : 1
        await addToCartReq(productId, quantity)
        const size = await cartSizeReq()
        dispatch(setCartSize(size))
        dispatch(setSnackbarState({
            severity: "success",
            msg: "Added to cart!"
        }));
    }

    // Makes API requests for each menu action
    const execAction = (menuAction) => {
        switch (menuAction) {
            case "shoppingCart":
                updateCart()
                break;
            case "favorite":
                if (isFavorited) {
                    removeFromWishlistReq(productId).then(() => setIsFavorited(false));
                } else {
                    addToWishlistReq(productId).then(() => setIsFavorited(true));
                }
                break;
            default:
                break;
        }
    }

    const handleOpen = (tool) => {
        setActiveMenu(tool.alias);
        execAction(tool.alias);
        setOpen(tool.showDialog);
    };

    const handleClose = () => {
        setOpen(false);
        setActiveMenu(null);
    };

    return (
        <div className={["menu_container", classes.menu_width].join(' ')} style={props.style}>
            {tools.map((tool, index) => {
                return (
                    <div key={`${tool}-${index}`}>
                        <Tooltip title={tool.tooltip}>
                            <span key={`${tool}-${index}`} className="menu_icon_fade" onClick={() => {
                                handleOpen(tool)
                            }}>
                                <i className={[tool.icon_name, classes.menu_icon, `${isFavorited && tool.alias === "favorite" ? "favorite" : ""}`].join(' ')}></i>
                            </span>
                        </Tooltip>
                        <Dialog
                            aria-labelledby="add-to-cart-confirmation"
                            maxWidth="md"
                            open={open}
                        >
                            {activeMenu === "viewDetails" &&
                                <ViewDetail
                                    product={props.product}
                                    onClose={handleClose}
                                />
                            }
                        </Dialog>
                    </div>
                )
            })}
        </div >
    );
}

export default Menu;