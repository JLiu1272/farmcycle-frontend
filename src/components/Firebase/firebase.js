import app from 'firebase/app';
import 'firebase/auth'

// Custom Imports
import { reloadPage } from '../Common/UtilityFunc';
import * as ROUTES from '../../constants/routes';
import { SyncRegisterWithBackendReq } from '../../mixin/ApiCaller'
import { pushToHistory } from '../Common/UtilityFunc';

// Fire Base Configuration 
const firebaseConfig = {
    apiKey: process.env.REACT_APP_API_KEY,
    authDomain: process.env.REACT_APP_AUTH_DOMAIN,
    databaseURL: process.env.REACT_APP_DATABASE_URL,
    projectId: process.env.REACT_APP_PROJECT_ID,
    storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
    messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
    appId: process.env.REACT_APP_APP_ID,
    measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

class Firebase {
    constructor() {
        this.firebaseProj = app.initializeApp(firebaseConfig);
        this.auth = app.auth();
    }

    // Auth API 
    doCreateUserWithEmailAndPassword = async (email, password) =>
        this.auth.createUserWithEmailAndPassword(email, password);

    doSignInWithEmailAndPassword =
        (email, password) =>
            this.auth.signInWithEmailAndPassword(email, password);

    // Update user profile 
    // Can only update displayName and photoURL
    updateProfile = (data) =>
        this.auth.currentUser.updateProfile(data);

    updateEmail = (email) =>
        this.auth.currentUser.updateEmail(email);

    doSignOut = () => {
        this.auth.signOut();
        reloadPage(ROUTES.HOME)
    };

    doPasswordReset = email => {
        return this.auth.sendPasswordResetEmail(email)
    }

    doPasswordUpdate = password =>
        this.auth.currentUser.updatePassword(password);

    // Get an ID token, to be sent to the backend 
    // for authentication 
    getIdToken = async () => {
        const idToken = await this.auth.currentUser.getIdToken(true)
        return idToken;
    }

    sendEmailVerification = () => this.auth.currentUser.sendEmailVerification()

    loginWithGoogle = () => {
        var provider = new app.auth.GoogleAuthProvider()
        return this.auth.signInWithPopup(provider)
    }

    syncGoogleAuthWithBackend = async () => {
        await this.loginWithGoogle()
        const tokenID = await this.getIdToken()
        await SyncRegisterWithBackendReq(tokenID)
        pushToHistory(ROUTES.HOME)
    }
}

export default Firebase; 