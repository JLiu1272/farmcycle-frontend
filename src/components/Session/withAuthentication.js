import AuthUserContext from './context';
import React, { useState, useEffect } from 'react'

// Context 
import { withFirebase } from '../Firebase';

const withAuthentication = Component => withFirebase((props) => {
    const [authUser, setAuthUser] = useState(null)

    useEffect(() => {
        props.firebase.auth.onAuthStateChanged(
            authUser => {
                if (authUser) setAuthUser(authUser)
            }
        )
    })
    return (
        <AuthUserContext.Provider value={authUser}>
            <Component {...props} authUser={authUser} />
        </AuthUserContext.Provider>
    );
})

export default withAuthentication;