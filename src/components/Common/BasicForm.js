import PropTypes from 'prop-types';
import React from 'react';
import clsx from 'clsx';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';

// Custom Components 
import CustomSnackbar from '../Notifications/CustomSnackbar';
import theme from '../../constants/theme';

// Functions
import { addSpaces, camelCase } from '../Common/UtilityFunc';

const useStyles = makeStyles((theme) => ({
    msg: {
        display: "flex",
        justifyContent: "center"
    },
    margin: {
        margin: theme.spacing(3),
        marginTop: theme.spacing(2),
    },
    textField: {
        width: 'ch',
    },
    spacing: {
        lineHeight: 3.3,
    },
    signup_actions: {
        display: "flex",
        justifyContent: "space-between",
        width: 306,
        margin: theme.spacing(3),
    }
}));


const BasicForm = props => {
    const classes = useStyles();

    return (
        <div>
            <CustomSnackbar
                duration={6000}
                severity={props.msg.errorMsg ? "error" : "success"}
                setMsg={props.setMsg}
                msg={props.msg.errorMsg ? props.msg.errorMsg : props.msg.successMsg}
            />
            {props.formFields.map((field, index) => {
                return (
                    <div key={`${field.name}-${index}`} className={clsx(classes.margin, classes.textField)} >
                        <TextField
                            id={`textfield-${field.name}`}
                            type={field.type}
                            label={addSpaces(field.name)}
                            value={props.values[camelCase(field.name)]}
                            onChange={props.handleChange(camelCase(field.name))}
                            fullWidth={true}
                        />
                    </div>
                )
            })}
            <Box mt={2} className={classes.signup_actions}>
                <ThemeProvider theme={theme}>
                    <Button
                        onClick={props.submitForm}
                        variant="contained"
                        color="primary"
                        disabled={props.isFormInvalid}>
                        {props.actionLabel}
                    </Button>
                </ThemeProvider>
            </Box>
        </div >
    );
};

BasicForm.propTypes = {
    formFields: PropTypes.array.isRequired,
    submitForm: PropTypes.func.isRequired,
    msg: PropTypes.object.isRequired,
    setMsg: PropTypes.func.isRequired,
    actionLabel: PropTypes.string.isRequired,
    handleChange: PropTypes.func.isRequired,
    values: PropTypes.object.isRequired,
};

export default BasicForm;