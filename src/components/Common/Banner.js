import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core';
import { useSelector } from 'react-redux';


const Banner = (props) => {
    const { page_banner } = useSelector(state => state.staticImgs);

    const useStyle = makeStyles(() => ({
        banner_bg: {
            backgroundImage: `url(${page_banner})`,
            backgroundPosition: "center",
            backgroundRepeat: "no-repeat",
            width: "100%",
            height: 200
        },
        title: {
            display: "flex",
            justifyContent: "center",
            lineHeight: "200px",
            fontSize: 40
        },
    }))

    const classes = useStyle();

    return (
        <div className={classes.banner_bg}>
            <Typography className={classes.title}>
                {props.title}
            </Typography>
        </div>
    )
}

export default Banner;