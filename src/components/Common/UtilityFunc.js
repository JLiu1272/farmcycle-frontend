/**
 * This file is use for storing functions auxillary
 * functions. This allows the function to be created
 * globally.
 */
import history from '../../history';

/**
 * Given an alias name in the 
 * form of xx_xx, convert it to 
 * xx xx. (i.e first_name -> First Name)
 * @param {*} field 
 */
export const addSpaces = (field) => {
    let words = field.split("_");
    words = words.map(word => {
        return word.charAt(0).toUpperCase() + word.slice(1);
    })
    return words.join(' ');
}

/**
 * Given a string that has underscore as separator, 
 * camel case the string. 
 * i.e camel_case -> camelCase 
 * @param {} value 
 */
export const camelCase = (str) => {
    let words = str.split("_");
    words = words.map(word => {
        return word.charAt(0).toUpperCase() + word.slice(1);
    })
    let concat = words.join('');
    return concat.charAt(0).toLowerCase() + concat.slice(1);
}

/**
 * Validate to make sure that the 
 * field is not null. If it is convert to a
 * string
 */

export const nullToStr = (value) => {
    return value ? value : "";
}


/**
 * Jump to url, and push that action 
 * to history stack
 */
export const pushToHistory = (route) => {
    history.push(route);
}

export const reloadPage = (route) => {
    window.location = process.env.REACT_APP_WEB_DOMAIN + "/" + route
}
