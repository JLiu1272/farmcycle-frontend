import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import _ from 'lodash';

// Component 
import Product from '../Product/Product';

// API 
import { productsByIdsReq } from '../../mixin/ApiCaller'

const containerWidth = 16;
const useStyles = makeStyles(() => ({
    col: {
        flex: "1",
        padding: "1em",
        width: `${containerWidth}rem`,
    },
    search_root: {
        width: 1100,
        padding: "2em",
    },
    row: {
        display: "flex",
        flexFlow: "row wrap",
    },
}));

const ProductToRow = props => {
    const classes = useStyles();
    const [products, setProducts] = useState(props.products ? props.products : [])

    useEffect(() => {
        if (props.productsArr) {
            productsByIdsReq(props.productsArr).then(setProducts)
        }
    }, [props.productsArr])

    const productToRow = (numCol) => {
        let row = [];
        let col = [];

        products.forEach((product, index) => {
            if (index % numCol === 0 && index > 0) {
                row.push(col);
                col = [];
            }
            col.push(
                <div className={classes.col} key={`col-${index}`}>
                    <Product
                        product={product}
                        thumbDim={containerWidth - 3}
                        isTopDown={true} />
                </div>
            );
        })

        // If the last row have less than n columns 
        // we add padding, so that the items will align 
        // to the left  
        if (col.length > 0) {
            _.range(numCol - col.length).forEach((index) => {
                col.push(
                    <div key={`filler-${index}`} className={classes.col}></div>
                )
            })
            row.push(col);
        }
        return row;
    }

    return (
        <div className={classes.search_root}>
            {productToRow(props.numCols).map((row, index) => {
                return (
                    <div className={classes.row} key={`row-${index}`}>
                        {row}
                    </div>
                )
            })}
        </div>
    );
};

ProductToRow.propTypes = {
    numCols: PropTypes.number.isRequired,
    productsArr: PropTypes.array,
    products: PropTypes.array
};

export default ProductToRow;