import React from 'react';
import PropTypes from 'prop-types';
import Box from '@material-ui/core/Box';

/**
 * When all there are failure in the system, 
 * this is the default error page  
 * @param {*} props 
 */

const ErrorMsg = props => {
    return (
        <Box component="center" m={12}>
            {props.msg}
        </Box>
    );
};

ErrorMsg.propTypes = {
    msg: PropTypes.string,
};

export default ErrorMsg;