import React from 'react';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { makeStyles } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import clsx from 'clsx';

// Actions 
import { updateSearchword } from '../../actions/Search';
import { setDrawerState } from '../../actions/State';
import history from '../../history';

// Routes 
import * as ROUTES from '../../constants/routes';

const useStyle = makeStyles(() => ({
    search_btn: {
        width: 70,
    },
    search_input: {
        height: 50,
        borderTopLeftRadius: 30,
        borderBottomLeftRadius: 30,
    },
    searchbar_container: {
        margin: "0 auto",
        marginTop: 40,
    },
    mobile_searchbar_container: {
        marginLeft: 20,
        marginRight: 20,
    },
    desktop_searchbar_container: {
        marginLeft: 50,
        marginRight: 30,
        width: "30rem"
    }
}));

const Searchbar = (props) => {
    let searchword = useSelector(state => state.searchword);
    const textInput = React.createRef();
    const dispatch = useDispatch();
    const mobileSize = useMediaQuery('(max-width:920px)')
    const classes = useStyle();

    const handleNewSearchword = _.debounce(() => {
        if (textInput.current) {
            dispatch(updateSearchword(textInput.current.value));
        }
    }, 500);

    const handleSearchClicked = () => {
        if (textInput.current) {
            dispatch(updateSearchword(textInput.current.value));
        }
        dispatch(setDrawerState(false));
        history.push(ROUTES.SHOP);
    }

    return (
        <div className={clsx(
            classes.searchbar_container,
            mobileSize ?
                classes.mobile_searchbar_container :
                classes.desktop_searchbar_container
        )}>
            <InputGroup className="mb-3">
                <FormControl
                    className={classes.search_input}
                    ref={textInput}
                    onChange={handleNewSearchword}
                    aria-describedby={searchword}
                    aria-label={searchword}
                    placeholder="Search item here" />
                <InputGroup.Prepend>
                    <Button
                        variant="success"
                        className={classes.search_btn}
                        onClick={handleSearchClicked}
                        style={{
                            borderBottomRightRadius: 30,
                            borderTopRightRadius: 30
                        }}
                    >
                        <FontAwesomeIcon color="white" icon="search" />
                    </Button>
                </InputGroup.Prepend>
            </InputGroup>
        </div>
    );
}

export default Searchbar;


