import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { makeStyles, ThemeProvider, withStyles } from '@material-ui/core/styles';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import Box from '@material-ui/core/Box';
import ListItemText from '@material-ui/core/ListItemText';
import Badge from '@material-ui/core/Badge';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import AccountCircleIcon from '@material-ui/icons/AccountCircleRounded';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCartOutlined';
import FavoriteBorder from '@material-ui/icons/FavoriteBorder';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { withAuthentication } from '../Session';
import { withFirebase } from '../Firebase';
import { getReq } from '../../mixin/ApiWrapper';
import { pushToHistory } from '../Common/UtilityFunc';
import { updateUserInfo, setAccountActiveTab } from '../../actions/User'
import { reset } from '../../actions/State'

// Routes
import * as ROUTES from '../../constants/routes';
import * as API from '../../constants/api_routes';

// CSS 
import './Admin.css';
import '../../assets/common.css';
import theme from '../../constants/theme';
import { ListItem, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    account_btn: {
        position: 'relative',
    },
    dropdown: {
        position: 'absolute',
        top: 40,
        right: 0,
        left: 0,
        width: 160,
        zIndex: 4,
        boxShadow: "0 0 5px 2px #c4bdbc",
        borderRadius: 3,
        backgroundColor: theme.palette.background.paper,
    },
    dropdown_icon: {
        minWidth: 35,
    },
    username: {
        padding: theme.spacing(1)
    }
}));

const StyledBadge = withStyles((theme) => ({
    badge: {
        right: -3,
        top: 13,
        border: `2px solid ${theme.palette.background.paper}`,
        padding: '0 4px',
    },
}))(Badge);

const AccountDropdown = withFirebase((props) => {
    const classes = useStyles();
    const [selectedIndex, setSelectedIndex] = useState(null);
    const { displayName } = useSelector(state => state.user)
    const dispatch = useDispatch()

    const handleListItemClick = (event, index) => {
        setSelectedIndex(index);
        props.setOpen(false)
    };

    const logout = async () => {
        await props.firebase.doSignOut();
        await getReq(API.logout);
        dispatch(reset())
    }

    const dropdownOptions = [
        {
            "title": "Account",
            action: () => {
                dispatch(setAccountActiveTab("profile"))
                pushToHistory(ROUTES.ACCOUNT)
            },
            "icon": <AccountCircleIcon />
        },
        {
            "title": "Address",
            action: () => {
                dispatch(setAccountActiveTab("address"))
                pushToHistory(ROUTES.ACCOUNT)
            },
            "icon": <FontAwesomeIcon icon="address-card" />
        },
        {
            "title": "Logout",
            action: () => logout(),
            "icon": <FontAwesomeIcon icon="sign-out-alt" />
        }
    ]
    const dividerIntervals = [1]

    return (
        <div className={classes.dropdown}>
            <Typography variant="h6" component="center" classes={{ root: classes.username }}>
                Hi {displayName}!
            </Typography>
            <Divider />
            {
                dropdownOptions.map((option, index) => {
                    return <List component="nav" key={option.title} aria-label="account dropdown menu">
                        <ListItem
                            button
                            selected={selectedIndex === index}
                            onClick={(event) => {
                                if (option.action) option.action()
                                handleListItemClick(event, index)
                            }}
                        >
                            <ListItemIcon classes={{ root: classes.dropdown_icon }}>
                                {option.icon}
                            </ListItemIcon>
                            <ListItemText primary={option.title} />
                        </ListItem>
                        {(dividerIntervals.includes(index)) && <Divider />}
                    </List>
                })
            }
        </div>
    )
})

const AuthAdmin = (props) => {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const handleClick = () => {
        setOpen((prev) => !prev);
    };

    const handleClickAway = () => {
        setOpen(false);
    };

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <div className={classes.account_btn}>
                <Button color="primary" variant="contained" onClick={handleClick}>
                    Account
                    {open ? <ExpandLess /> : <ExpandMore />}
                </Button>
                {open ? (
                    <AccountDropdown setOpen={setOpen} />
                ) : null}
            </div>
        </ClickAwayListener>
    );
}

/**
 * This is what an unauthenticated user 
 * will see when accessing this page 
 * @param {*} props 
 */
const NonAuthAdmin = (props) => {
    const classes = useStyles()

    return (
        <div className={classes.account_btn}>
            <Button color="primary" variant="contained" onClick={() => pushToHistory(ROUTES.LOGIN)}>
                Log In
            </Button>
        </div>
    )
}

const AdminPrivillege = withAuthentication((props) => {
    const dispatch = useDispatch()
    const { displayName, email } = useSelector(state => state.user)

    if (props.authUser) {
        // Only update the store if there were changes 
        // to the value
        if (props.authUser.displayName !== displayName) {
            dispatch(updateUserInfo({
                "displayName": props.authUser.displayName
            }))
        } else if (props.authUser.email !== email) {
            dispatch(updateUserInfo({
                "email": props.authUser.email
            }))
        }
        return <AuthAdmin />
    }
    return <NonAuthAdmin />
})

const Admin = (props) => {
    const { size } = useSelector(state => state.cart);

    return (
        <ThemeProvider theme={theme}>
            <Box display="flex" flexWrap="wrap" ml={4} m={3}>
                <AdminPrivillege />
                <Box ml={1} mt="-6px">
                    <IconButton aria-label="Favorites" onClick={() => pushToHistory(ROUTES.WISHLIST)}>
                        <FavoriteBorder />
                    </IconButton>
                    <IconButton aria-label="cart" onClick={() => pushToHistory(ROUTES.SHOPPING_CART)}>
                        <StyledBadge badgeContent={size} color="primary">
                            <ShoppingCartIcon />
                        </StyledBadge>
                    </IconButton>
                </Box>
            </Box>
        </ThemeProvider >
    );

}

export default Admin;
