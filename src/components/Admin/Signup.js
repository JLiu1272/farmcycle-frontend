import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import GoogleButton from 'react-google-button'
import { useDispatch } from 'react-redux';

// Custom Components 
import Banner from '../Common/Banner';
import AlertMsg from '../Notifications/AlertMsg';

// Constants
import theme from '../../constants/theme';
import * as ROUTES from '../../constants/routes';

// Functions 
import { withFirebase } from '../Firebase';
import { SyncRegisterWithBackendReq } from '../../mixin/ApiCaller'
import { updateUserInfo } from '../../actions/User'
import { pushToHistory } from '../Common/UtilityFunc';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 326,
        margin: "0 auto",
        display: "block",
        marginTop: 30,
        marginBottom: 30,
        textAlign: "center",
    },
    error_msg: {
        display: "flex",
        justifyContent: "center"
    },
    margin: {
        margin: theme.spacing(1),
    },
    textField: {
        width: '30ch',
    },
    spacing: {
        lineHeight: 3.3,
        marginTop: 30,
    },
}));

/**
 * Given an alias name in the 
 * form of xx_xx, convert it to 
 * xx xx. (i.e first_name -> First Name)
 * @param {*} field 
 */
const addSpaces = (field) => {
    let words = field.split("_");
    words = words.map(word => {
        return word.charAt(0).toUpperCase() + word.slice(1);
    })
    return words.join(' ');
}

const SignUpFormBase = (props) => {
    const formFields = [
        {
            field_name: "name",
            isInvalid: () => { return values.name === '' },
            helper_text: "Name cannot be empty",
        },
        {
            field_name: "email",
            isInvalid: () => { return values.email === '' },
            helper_text: "Email cannot be empty",
        },
        {
            field_name: "password",
            isInvalid: () => {
                return values.password !== values.password_confirmation || values.password === '';
            },
            helper_text: "Passwords do not match or password is empty",
        },
        {
            field_name: "password_confirmation",
            isInvalid: () => {
                return values.password !== values.password_confirmation || values.password_confirmation === '';
            },
            helper_text: "Passwords do not match or password is empty",
        },
    ];

    let initValue = {};
    let initChanged = {};
    formFields.forEach(({ field_name }) => {
        initValue[field_name] = "";
        initChanged[field_name] = false;
    })

    const classes = useStyles();
    const dispatch = useDispatch()
    const [values, setValues] = useState(initValue);
    const [errorMsg, setErrorMsg] = useState(null);
    const [changed, setChanged] = useState(initChanged);
    const [showPassword, setShowPassword] = useState(false);

    const handleChange = (prop) => (event) => {
        setChanged({ ...changed, [prop]: true });
        setValues({ ...values, [prop]: event.target.value });
    };

    const handleShowPassword = () => {
        setShowPassword(!showPassword);
    }

    const isFormInvalid = () => {
        return values.password !== values.password_confirmation
            || values.password === ''
            || values.email === ''
            || values.name === ''
    }

    const submitForm = (event) => {
        const { name, email, password } = values;
        event.preventDefault();

        props.firebase.doCreateUserWithEmailAndPassword(email, password)
            .then(() => {
                return props.firebase.updateProfile({
                    displayName: name,
                });
            })
            .then(() => {
                return props.firebase.getIdToken()
            })
            .then(token => {
                return SyncRegisterWithBackendReq(token)
            })
            .then(() => {
                dispatch(updateUserInfo({
                    displayName: name,
                }))
                pushToHistory(ROUTES.HOME)
            })
            .catch(error => {
                setErrorMsg(error.message);
            });

    }

    const signUpWithGoogle = () => {
        props.firebase.loginWithGoogle()
            .then(() => {
                return props.firebase.getIdToken()
            })
            .then((token) => {
                return SyncRegisterWithBackendReq(token)
            })
            .then(response => {
                if (typeof response === "string") {
                    setErrorMsg(response)
                    return
                }
                pushToHistory(ROUTES.HOME)
            })
            .catch(err => {
                setErrorMsg(err.message)
            })
    }

    return (
        <ThemeProvider theme={theme}>
            <Box>
                {errorMsg &&
                    <div className={classes.error_msg}>
                        <AlertMsg
                            addTimer={true}
                            msg={errorMsg}
                            color="red" />
                    </div>
                }
                {formFields.map(({ field_name, isInvalid, helper_text }, index) => {
                    return (
                        <FormControl
                            key={`${field_name}-${index}`}
                            className={clsx(classes.margin, classes.textField, classes.spacing)}
                            variant="outlined">
                            <TextField
                                autoComplete={field_name}
                                id={`outlined-adornment-${field_name}`}
                                type={(field_name === "password" || field_name === "password_confirmation") && !showPassword ? 'password' : 'text'}
                                placeholder={addSpaces(field_name)}
                                error={changed[field_name] && isInvalid()}
                                helperText={changed[field_name] && isInvalid() ? helper_text : ""}
                                variant="outlined"
                                value={values.field_name}
                                onChange={handleChange(field_name)}
                                InputProps={{
                                    endAdornment: (
                                        (field_name === "password" || field_name === "password_confirmation") &&
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="Toggle password visibility"
                                                onClick={handleShowPassword}
                                                edge="end"
                                            >
                                                {showPassword ? <VisibilityOff /> : <Visibility />}
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                            />
                        </FormControl>
                    )
                })}
                <Box m={1} mb={2}>
                    <Button
                        onClick={submitForm}
                        fullWidth={true}
                        variant="contained"
                        color="primary"
                        disabled={isFormInvalid()}>
                        Signup
                    </Button>
                </Box>
                <Divider />
                <Box m={2} component="center">
                    <GoogleButton
                        label="Sign up with Google"
                        onClick={signUpWithGoogle}
                    />
                </Box>
            </Box>
        </ThemeProvider>
    )
}

const Signup = props => {
    const classes = useStyles();

    return (
        <div>
            <Banner title="Signup" />
            <div className={classes.root}>
                <Typography>
                    Please fill in the details below
                </Typography>
                <SignUpForm />
            </div>
        </div>
    );
};

/**
 * If the user is already logged in, 
 * then we redirect them to the homepage, 
 * otherwise we allow them to log in
 * @param {*} props 
 */
const SignupMain = props => {
    return (
        <Signup />
    );

}

const SignUpForm = withFirebase(SignUpFormBase);

export default SignupMain;