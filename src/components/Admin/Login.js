import React, { useState } from 'react';
import { makeStyles, ThemeProvider } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import GoogleButton from 'react-google-button'

// Components
import Banner from '../Common/Banner';
import history from '../../history';
import AlertMsg from '../Notifications/AlertMsg';
import theme from '../../constants/theme';

// Routes 
import * as ROUTES from '../../constants/routes';
import { setIsAuthedStatus } from '../../actions/User'
import { withFirebase } from '../Firebase';
import { withAuthentication } from '../Session'
import { SyncLoginWithBackendReq } from '../../mixin/ApiCaller'
import { useDispatch } from 'react-redux';

// CSS 
import '../../assets/common.css'

const useStyles = makeStyles((theme) => ({
    root: {
        width: 362,
        margin: "0 auto",
        display: "block",
        marginTop: 30,
        marginBottom: 30,
        textAlign: "center",
    },
    alert_msg: {
        display: "flex",
        justifyContent: "center"
    },
    login_root: {
        '& > div': {
            margin: 20,
        }
    },
    login_btn: {
        marginLeft: 20,
        marginRight: 20,
    }
}));

const LoginFormBase = withAuthentication(props => {
    const classes = useStyles();

    const initValue = {
        password: '',
        showPassword: false,
        email: '',
    };
    const [values, setValues] = useState(initValue);
    const [errorMsg, setErrorMsg] = useState(null);
    const [successMsg, setSuccessMsg] = useState(null);
    const dispatch = useDispatch()

    const resetAlertMsgs = () => {
        setErrorMsg(null)
        setSuccessMsg(null)
    }

    const handleChange = (prop) => (event) => {
        setValues({ ...values, [prop]: event.target.value });
        resetAlertMsgs()
    };

    const handleClickShowPassword = () => {
        setValues({ ...values, showPassword: !values.showPassword });
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const onSubmit = event => {
        event.preventDefault();
        const { email, password } = values;

        var authenticate = async () => {
            const authenticated = await props.firebase.doSignInWithEmailAndPassword(email, password)
                .then(() => {
                    return props.firebase.getIdToken();
                })
                .then(async (idToken) => {
                    const response = await SyncLoginWithBackendReq(idToken)
                    if (response === "Failed to fetch") {
                        setErrorMsg(response);
                        return false;
                    }
                    return true;
                })
                .catch((error) => {
                    setErrorMsg(error.message);
                    return false;
                })

            if (authenticated) {
                dispatch(setIsAuthedStatus(true))
                history.push(ROUTES.HOME);
            }
        }
        authenticate();
    }

    const HandlePasswordReset = () => {
        const email = values["email"]
        if (email === "") {
            setErrorMsg("You must fill the email in order to perform password reset")
            return
        }
        props.firebase.doPasswordReset(email)
            .then(() => {
                setSuccessMsg(`Reset Password Link successfully sent to ${email}. Please check spam if email not in regular mailbox`)
            })
            .catch(err => setErrorMsg(err.message))
    }

    return (
        <ThemeProvider theme={theme}>
            <Box display="flex" flexDirection="column" justifyContent="center" className={classes.login_root}>
                {(errorMsg || successMsg) &&
                    <div className={classes.alert_msg}>
                        <AlertMsg
                            addTimer={false}
                            msg={errorMsg ? errorMsg : successMsg}
                            color={errorMsg ? "red" : "green"} />
                    </div>
                }
                <FormControl variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-email">Email</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-email"
                        type="text"
                        autoComplete="username"
                        value={values.email}
                        onChange={handleChange('email')}
                        labelWidth={70}
                    />
                </FormControl>
                <FormControl variant="outlined">
                    <InputLabel htmlFor="outlined-adornment-password">Password</InputLabel>
                    <OutlinedInput
                        id="outlined-adornment-password"
                        autoComplete="password"
                        type={values.showPassword ? 'text' : 'password'}
                        value={values.password}
                        onChange={handleChange('password')}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={handleClickShowPassword}
                                    onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                >
                                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                                </IconButton>
                            </InputAdornment>
                        }
                        labelWidth={70}
                    />
                </FormControl>
                <Button variant="contained" color="primary" onClick={onSubmit} className={classes.login_btn}>
                    Login
                </Button>
                <Box mt={1}>
                    Don't have an account? <a className="a_tag" href={ROUTES.SIGN_UP}>Register</a>
                    <br />
                    Forgot your password? <span className="a_tag" onClick={HandlePasswordReset}>Reset it</span>
                </Box>
                <Divider />
                <Box component="center" m={2}>
                    <GoogleButton onClick={props.firebase.syncGoogleAuthWithBackend} />
                </Box>
            </Box>
        </ThemeProvider>
    )
})

const Login = props => {
    const classes = useStyles();

    return (
        <div>
            <Banner title="Login" />
            <div className={classes.root}>
                <Typography>
                    Please login using account detail bellow.
                </Typography>
                <LoginForm />
            </div>
        </div>
    )
};

const LoginForm = withFirebase(LoginFormBase);

export default Login;