/**
 * Functions that handles payments calculations 
 */
import _ from 'lodash'
import { loadStripe } from '@stripe/stripe-js'

// API 
import { makeCheckoutSessionReq } from './ApiCaller'

// Stripe Payment 
const stripePromise = loadStripe(process.env.REACT_APP_STRIPE_KEY)

/**
 * Calculate the final price of this transaction
 * @param {*} cartItems, products 
 */
export const totalPrice = (cartItems) => {
    const totalCost = _.reduce(cartItems, (total, item) => {
        return total + item.price * item.quantity;
    }, 0);
    return totalCost;
}

/**
 * Adds a dollar sign, and if the value is a negative 
 * add the negative value
 */
export const toPriceStr = (isNegative, price) => {
    const negativeSign = isNegative ? "-" : "";
    return `${negativeSign}$${price}`;
}

/**
 * Calculate the discounted price  
 * @param {*} price 
 * @param {*} saleAmt (in percentage, i.e 5 = 5% off) 
 */
export const calcSalePrice = (price, saleAmt) => {
    return saleAmt
        ? (price - (price * (saleAmt / 100))).toFixed(2)
        : price
}

// Cherry pick which key,value pairs we want from cartItems 
// and return a new cartItems array with the ones we want included 
// @param 
//  - keysToInclude - keys that we want in the restructured result 
export const restructureCartItems = (cartItems, keysToInclude) => {
    const cartItemsModified = _.reduce(cartItems, (result, product, productId) => {
        // Creates a new item with key,pair value specified in keysToInclude
        let itemCpy = {}
        keysToInclude.forEach(includeField => {
            itemCpy[includeField] = product[includeField]
        });
        result.push(itemCpy)
        return result
    }, [])
    return cartItemsModified
}

export const makePayment = async (items, currency, keysToInclude) => {
    const stripe = await stripePromise
    const modifiedItems = restructureCartItems(items, keysToInclude)
    const response = await makeCheckoutSessionReq(currency, modifiedItems)

    // If type of response is a string, it means that
    // there was an error when processing the payment 
    if (typeof response === "string") {
        throw new Error(response)
    }

    const result = await stripe.redirectToCheckout({
        sessionId: response.sessionID
    })

    if (result.error) {
        throw new Error(result.error.message)
    }

    return result
}