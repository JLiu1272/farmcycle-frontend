// A wrapper for difference requests 
export async function postReq(url, data) {
    const response = await fetch(url, {
        method: 'post', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify(data)
    }).then(async response => {
        const contentType = response.headers.get("content-type");
        // Check to see if the response in the post request is json, 
        // if it is, return the data, else return text
        if (contentType && contentType.indexOf("application/json") !== -1) {
            const data = await response.json();
            return data;
        } else {
            const text = await response.text();
            return text;
        }
    })
    return response;
}

export async function getReq(url) {
    const response = await fetch(url, {
        method: 'get', // *GET, POST, PUT, DELETE, etc.
        headers: {
            'Content-Type': 'application/json',
        },
        credentials: 'include',
    }).then(async response => {
        const contentType = response.headers.get("content-type");
        // Check to see if the response in the post request is json, 
        // if it is, return the data, else return text
        if (contentType && contentType.indexOf("application/json") !== -1) {
            const data = await response.json();
            return data;
        } else {
            const text = await response.text();
            return text;
        }
    }).catch(function (err) {
        return err.message;
    });
    return response;
}