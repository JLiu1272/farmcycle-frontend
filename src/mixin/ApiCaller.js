// API Routes
import * as API from '../constants/api_routes';

// Actions
import { postReq, getReq } from './ApiWrapper';

// Cart API

/**
 * addToCartReq 
 * @param {*} productId 
 * @param {*} dispatch 
 */
export async function addToCartReq(productId, quantity) {
    const data = {
        "items": {}
    }
    data.items[productId] = quantity
    await postReq(API.addToCart, data).catch(err => console.log(err))
}

export async function removeFromCartReq(productId, quantity) {
    const data = {
        "items": {}
    }
    data.items[productId] = quantity
    await postReq(API.removeFromCart, data).catch(err => console.log(err))
}

export async function updateQuantityReq(productId, quantity) {
    const data = {
        "items": {}
    }
    data.items[productId] = quantity
    await postReq(API.updateCartQuantity, data).catch(err => console.log(err))
}

export async function productDetailsInCartReq() {
    const productDetails = await getReq(API.productDetailsInCart).catch(err => { console.log(err.message) })
    return typeof productDetails === "string" ? [] : productDetails;
}

export async function cartSizeReq() {
    const cartSize = await getReq(API.cartSize).catch(err => {
        console.error(err.message)
    })
    return typeof cartSize === "string" ? 0 : cartSize.Size
}

// Wishlist API 
export async function removeFromWishlistReq(productId) {
    const data = {
        "productIds": []
    }
    data.productIds = [productId]
    await postReq(API.removeFromWishlist, data).catch(err => console.log(err))
}

export async function addToWishlistReq(productId) {
    const data = {
        "productIds": []
    }
    data.productIds = [productId]
    await postReq(API.addToWishlist, data).catch(err => console.log(err))
}

export async function isFavoritedByCustomerReq(productId) {
    const data = {
        "productIds": []
    }
    data.productIds = [productId]
    const response = await postReq(API.isFavorited, data).catch(err => console.log(err))
    return response["isFavorited"];
}

export async function wishlistByCustomerIdReq() {
    const wishlistProducts = await getReq(API.wishlistByCustomerId).catch(err => {
        console.error(err.message)
    })
    return wishlistProducts;
}

// Product Rendering 
export async function allProductsByTagReq() {
    const products = await getReq(API.allProductsByTag).catch(err => {
        console.error(err.message)
    })
    return typeof products === "string" ? {} : products;
}

export async function productByIdReq(productId) {
    const data = {
        "productIds": [productId]
    }
    const products = await postReq(API.productsByIds, data).catch(err => {
        console.error(err.message)
    })
    return products === null || products === "Failed to fetch" ? products : products[0]
}

export async function productsByIdsReq(productIds) {
    const data = {
        "productIds": productIds
    }
    const products = await postReq(API.productsByIds, data).catch(err => {
        console.error(err.message)
    })
    return typeof products === "string" ? [] : products
}

export async function productsByKeywordReq(keyword) {
    console.log(keyword)
    const data = {
        "keyword": keyword
    }
    const products = await postReq(API.productsByKeyword, data).catch(err => {
        console.error(err.message)
    })
    return products
}

// Farmer 
export async function farmerInfoByIdReq(farmerId) {
    const data = { "farmerId": farmerId }
    const farmerInfo = await postReq(API.farmerInfoById, data).catch(err => {
        console.error(err.message)
    })
    return farmerInfo;
}

// Customer Profile 

// addShippingAddressReq returns false if an error occurs, and 
// will return true if the operation was successful  
export async function addShippingAddressReq(address) {
    const res = await postReq(API.addShippingAddress, address).catch(err => {
        console.error(err.message)
    })
    return res
}

export async function shippingAddressesReq() {
    const addresses = await getReq(API.shippingAddresses).catch(err => {
        console.error(err.message)
    })
    return addresses
}

export async function removeShippingAddressReq(addressLine1) {
    const data = {
        "addressLine1": addressLine1
    }
    const res = await postReq(API.removeShippingAddress, data).catch(err => {
        console.error(err.message)
    })
    return res
}

export async function updatePhoneNumberReq(newPhoneNumber) {
    const data = {
        "phoneNumber": newPhoneNumber
    }
    const response = await postReq(API.updatePhoneNumber, data)
    return response
}

export async function phoneNumberByIdReq() {
    const response = await getReq(API.phoneNumberById)
    return response
}

// Payment
export async function makeCheckoutSessionReq(currency, items) {
    const data = {
        "currency": currency,
        "items": items
    }

    const response = await postReq(API.makeCheckoutSession, data).catch(err => {
        console.error(err.message)
    })
    return response
}

// Admin 
export async function isAuthReq() {
    const res = await getReq(API.isAuth).catch(err => {
        console.error(err.message)
    })
    return typeof res === "string" ? false : res.isAuth
}

// Sync register with backend and firebase 
export async function SyncRegisterWithBackendReq(token) {
    const data = {
        id_token: token
    }
    const response = await postReq(API.register, data).catch(err => {
        console.error(err.message)
    })
    return response
}

export async function SyncLoginWithBackendReq(token) {
    const data = {
        id_token: token
    }
    const response = await postReq(API.login, data).catch(err => {
        console.error(err.message)
    })
    return response
}

export async function confirmCheckoutReq(receiptContent) {
    const response = await postReq(API.confirmCheckout, receiptContent).catch(err => {
        console.error(err.message)
    })
    return response
}

